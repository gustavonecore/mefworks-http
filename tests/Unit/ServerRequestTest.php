<?php namespace mef\Http\Test\Unit;

use mef\Http\ServerRequest;
use mef\Http\StringStream;
use mef\Http\UploadedFile;

/**
 * @coversDefaultClass \mef\Http\ServerRequest
 */
class ServerRequestTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::fromGlobals
	 * @covers ::__construct
	 * @covers ::filterArray
	 * @covers ::decodeBody
	 * @covers ::getServerParams
	 * @covers ::getCookieParams
	 * @covers ::getQueryParams
	 * @covers ::getParsedBody
	 * @covers ::getAttributes
	 * @covers ::getUploadedFiles
	 */
	public function testFromGlobals()
	{
		$server = [
			'REQUEST_METHOD' => 'POST',
			'HTTPS' => 'on',
			'REQUEST_URI' => '/?get=a',
			'SERVER_PROTOCOL' => '1.2',
			'HTTP_HOST' => 'localhost'
		];

		$query = ['get' => 'a'];

		$post = ['post' => 'b'];

		$cookies = ['cookie' => 'c'];

		$attachments = [
			'attachment' => [
				'tmp_name' => '/tmp/foo.png',
				'name' => 'foo.png',
				'type' => 'image/png',
				'error' => 0,
				'size' => 0
			]
		];

		$request = ServerRequest::fromGlobals(
			new StringStream('Hello'),
			$server,
			$query,
			$post,
			$cookies,
			$attachments
		);

		$this->assertTrue($request instanceof ServerRequest);
		$this->assertSame($server, $request->getServerParams());
		$this->assertSame($cookies, $request->getCookieParams());
		$this->assertSame($query, $request->getQueryParams());
		$this->assertSame($post, $request->getParsedBody());
		$this->assertSame([], $request->getAttributes());
		$this->assertSame(1, count($request->getUploadedFiles()));
	}

	/**
	 * @covers ::fromGlobals
	 * @covers ::decodeBody
	 */
	public function testFromGlobalsWithJson()
	{
		$server = [
			'REQUEST_METHOD' => 'POST',
			'REQUEST_URI' => '/',
			'HTTP_CONTENT_TYPE' => 'application/json',
			'HTTP_HOST' => 'localhost'
		];

		$query = [];
		$post = [];
		$cookies = [];
		$attachments = [];

		$encodedData = ['foo' => 'bar'];

		$request = ServerRequest::fromGlobals(
			new StringStream(json_encode($encodedData)),
			$server,
			$query,
			$post,
			$cookies,
			$attachments
		);

		$this->assertTrue($request instanceof ServerRequest);
		$this->assertSame($server, $request->getServerParams());
		$this->assertSame($cookies, $request->getCookieParams());
		$this->assertSame($query, $request->getQueryParams());
		$this->assertSame($encodedData, $request->getParsedBody());
		$this->assertSame([], $request->getAttributes());
		$this->assertSame(0, count($request->getUploadedFiles()));
	}

	/**
	 * @covers ::fromGlobals
	 * @covers ::decodeBody
	 *
	 * @expectedException \RuntimeException
	 */
	public function testFromGlobalsWithMalformattedJson()
	{
		$server = [
			'REQUEST_METHOD' => 'POST',
			'REQUEST_URI' => '/',
			'HTTP_CONTENT_TYPE' => 'application/json',
			'HTTP_HOST' => 'localhost'
		];

		ServerRequest::fromGlobals(
			new StringStream("invalid: json"),
			$server
		);
	}

	/**
	 * @covers ::fromGlobals
	 * @covers ::decodeBody
	 *
	 * @expectedException \RuntimeException
	 */
	public function testFromGlobalsWithInvalidJsonType()
	{
		$server = [
			'REQUEST_METHOD' => 'POST',
			'REQUEST_URI' => '/',
			'HTTP_CONTENT_TYPE' => 'application/json',
			'HTTP_HOST' => 'localhost'
		];

		ServerRequest::fromGlobals(
			new StringStream(json_encode("string")),
			$server
		);
	}

	/**
	 * @covers ::fromGlobals
	 * @covers ::decodeBody
	 *
	 * @expectedException \RuntimeException
	 */
	public function testFromGlobalsWithValidButNullJson()
	{
		$server = [
			'REQUEST_METHOD' => 'POST',
			'REQUEST_URI' => '/',
			'HTTP_CONTENT_TYPE' => 'application/json',
			'HTTP_HOST' => 'localhost'
		];

		ServerRequest::fromGlobals(
			new StringStream(json_encode(null)),
			$server
		);
	}

	/**
	 * @covers ::fromGlobals
	 * @covers ::decodeBody
	 *
	 * @expectedException \RuntimeException
	 */
	public function testFromGlobalsWithJsonAndPost()
	{
		$server = [
			'REQUEST_METHOD' => 'POST',
			'REQUEST_URI' => '/',
			'HTTP_CONTENT_TYPE' => 'application/json',
			'HTTP_HOST' => 'localhost'
		];

		ServerRequest::fromGlobals(
			new StringStream(json_encode(['foo' => 'bar'])),
			$server,
			[], // get
			['foo' => 'bar'] // post
		);
	}

	/**
	 * @covers ::withCookieParams
	 * @covers ::getCookieParams
	 */
	public function testWithCookieParams()
	{
		$request = new ServerRequest(['uri' => 'https:']);
		$this->assertTrue($request instanceof ServerRequest);

		$request2 = $request->withCookieParams(['foo' => 'bar']);
		$this->assertTrue($request2 instanceof ServerRequest);
		$this->assertNotSame($request, $request2);

		$this->assertSame(['foo' => 'bar'], $request2->getCookieParams());
	}

	/**
	 * @covers ::withCookieParams
	 * @covers ::getCookieParams
	 */
	public function testWithSameCookieParams()
	{
		$request = new ServerRequest(['uri' => 'https:']);
		$this->assertTrue($request instanceof ServerRequest);

		$request2 = $request->withCookieParams([]);
		$this->assertTrue($request2 instanceof ServerRequest);
		$this->assertSame($request, $request2);

		$this->assertSame([], $request2->getCookieParams());
	}

	/**
	 * @covers ::withQueryParams
	 * @covers ::getQueryParams
	 */
	public function testWithQueryParams()
	{
		$request = new ServerRequest(['uri' => 'https:']);
		$this->assertTrue($request instanceof ServerRequest);

		$request2 = $request->withQueryParams(['foo' => 'bar']);
		$this->assertTrue($request2 instanceof ServerRequest);
		$this->assertNotSame($request, $request2);

		$this->assertSame(['foo' => 'bar'], $request2->getQueryParams());
	}

	/**
	 * @covers ::withQueryParams
	 * @covers ::getQueryParams
	 */
	public function testWithSameQueryParams()
	{
		$request = new ServerRequest(['uri' => 'https:']);
		$this->assertTrue($request instanceof ServerRequest);

		$request2 = $request->withQueryParams([]);
		$this->assertTrue($request2 instanceof ServerRequest);
		$this->assertSame($request, $request2);

		$this->assertSame([], $request2->getQueryParams());
	}

	/**
	 * @covers ::withUploadedFiles
	 * @covers ::getUploadedFiles
	 */
	public function testWithUploadedFiles()
	{
		$request = new ServerRequest(['uri' => 'https:']);
		$this->assertTrue($request instanceof ServerRequest);

		$file = new UploadedFile('/tmp/foo.png');

		$request2 = $request->withUploadedFiles([$file]);
		$this->assertTrue($request2 instanceof ServerRequest);
		$this->assertNotSame($request, $request2);
		$this->assertSame($file, $request2->getUploadedFiles()[0]);
	}

	/**
	 * @covers ::withUploadedFiles
	 * @covers ::getUploadedFiles
	 */
	public function testWithSameUploadedFiles()
	{
		$request = new ServerRequest(['uri' => 'https:']);
		$this->assertTrue($request instanceof ServerRequest);

		$request2 = $request->withUploadedFiles([]);
		$this->assertTrue($request2 instanceof ServerRequest);
		$this->assertSame($request, $request2);
		$this->assertSame([], $request2->getUploadedFiles());
	}

	/**
	 * @covers ::withUploadedFiles
	 *
	 * @expectedException \InvalidArgumentException
	 */
	public function testWithInvalidUploadedFile()
	{
		$request = new ServerRequest(['uri' => 'https:']);
		$this->assertTrue($request instanceof ServerRequest);

		$request->withUploadedFiles(['foo']);
	}

	/**
	 * @covers ::withParsedBody
	 * @covers ::getParsedBody
	 */
	public function testWithParsedBody()
	{
		$request = new ServerRequest(['uri' => 'https:']);
		$this->assertTrue($request instanceof ServerRequest);

		$request2 = $request->withParsedBody(['foo' => 'bar']);
		$this->assertTrue($request2 instanceof ServerRequest);
		$this->assertNotSame($request, $request2);
		$this->assertSame(['foo' => 'bar'], $request2->getParsedBody());
	}

	/**
	 * @covers ::withParsedBody
	 * @covers ::getParsedBody
	 */
	public function testWithSameParsedBody()
	{
		$request = new ServerRequest(['uri' => 'https:']);
		$this->assertTrue($request instanceof ServerRequest);

		$request2 = $request->withParsedBody(null);
		$this->assertTrue($request2 instanceof ServerRequest);
		$this->assertSame($request, $request2);
		$this->assertSame(null, $request2->getParsedBody());
	}

	/**
	 * @covers ::withParsedBody
	 *
	 * @expectedException \InvalidArgumentException
	 */
	public function testWithInvalidParsedBody()
	{
		$request = new ServerRequest(['uri' => 'https:']);
		$this->assertTrue($request instanceof ServerRequest);

		$request->withParsedBody('invalid');
	}

	/**
	 * @covers ::getAttribute
	 * @covers ::withAttribute
	 */
	public function testGetAttribute()
	{
		$request = new ServerRequest(['uri' => 'https:']);
		$this->assertTrue($request instanceof ServerRequest);
		$this->assertTrue($request->getAttribute('invalid', true));

		$request2 = $request->withAttribute('foo', 'bar');
		$this->assertTrue($request2 instanceof ServerRequest);
		$this->assertNotSame($request, $request2);
		$this->assertSame('bar', $request2->getAttribute('foo'));
	}

	/**
	 * @covers ::getAttribute
	 * @covers ::withAttribute
	 */
	public function testGetSameAttribute()
	{
		$request = new ServerRequest(['uri' => 'https:']);
		$this->assertTrue($request instanceof ServerRequest);
		$request2 = $request->withAttribute('foo', null);
		$this->assertTrue($request2 instanceof ServerRequest);
		$this->assertSame($request, $request2);
		$this->assertSame(null, $request2->getAttribute('foo'));
	}

	/**
	 * @covers ::withoutAttribute
	 * @covers ::withAttribute
	 * @covers ::getAttribute
	 */
	public function testWithoutAttribute()
	{
		$request = new ServerRequest(['uri' => 'https:']);
		$this->assertTrue($request instanceof ServerRequest);

		$request = $request->withAttribute('foo', 'bar');
		$request2 = $request->withoutAttribute('foo');
		$this->assertTrue($request2 instanceof ServerRequest);
		$this->assertNotSame($request, $request2);
		$this->assertSame(null, $request2->getAttribute('foo'));
	}

	/**
	 * @covers ::withoutAttribute
	 * @covers ::withAttribute
	 * @covers ::getAttribute
	 */
	public function testWithoutMissingAttribute()
	{
		$request = new ServerRequest(['uri' => 'https:']);
		$this->assertTrue($request instanceof ServerRequest);

		$request2 = $request->withoutAttribute('foo');
		$this->assertTrue($request2 instanceof ServerRequest);
		$this->assertSame($request, $request2);
		$this->assertSame(null, $request2->getAttribute('foo'));
	}
}