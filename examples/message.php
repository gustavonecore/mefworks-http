<?php namespace mef\Http\Example;

require_once __DIR__ . '/../vendor/autoload.php';

use mef\Http\Message;
use mef\Http\StringStream;

$message = new Message([
	'body' => new StringStream('<h1>Hello</h1>'),
	'headers' => ['Content-Type' => 'text/html']
]);

echo "The default protocol version is {$message->getProtocolVersion()}", PHP_EOL;

// Note that headers are an array of string arrays
echo "The headers:", PHP_EOL;
var_dump($message->getHeaders());

// But you can use getHeaderLine to make sure it's a string
// And all header keys are case insensitive.
echo "Content-Type is {$message->getHeaderLine('content-type')}", PHP_EOL;

// To replace a header use withHeader
$message = $message->withHeader('Content-Type', 'text/plain');
echo "Content-Type is now {$message->getHeaderLine('content-type')}", PHP_EOL;

// To add a header use withAddedHeader
// When there are muliple headers, getHeaderLine will join them with a comma
$message = $message->withAddedHeader('Content-Type', 'application/unknown');
echo "Content-Type is now {$message->getHeaderLine('content-type')}", PHP_EOL;

// To remove a header use withoutHeader
$message = $message->withoutHeader('Content-Type');
echo "Content-Type is now", PHP_EOL;
var_dump($message->getHeaderLine('Content-Type'));

// Note that the body is actually a stream. In most cases, you should access
// it via the stream interface.
echo "The body is {$message->getBody()}", PHP_EOL;

echo "The body five bytes at a time:", PHP_EOL;
while (($body = $message->getBody()->read(5)) !== '')
{
	echo $body;
	usleep(500000);
}
echo PHP_EOL;