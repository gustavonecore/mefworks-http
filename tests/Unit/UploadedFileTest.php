<?php namespace mef\Http\Test\Unit;

use mef\Http\UploadedFile;
use Psr\Http\Message\StreamInterface;
use org\bovigo\vfs\vfsStream;

/**
 * @coversDefaultClass \mef\Http\UploadedFile
 */
class UploadedFileTest extends \PHPUnit_Framework_TestCase
{
	public function setup()
	{
		$this->vfs = vfsStream::setup('tmp');
		$this->upload = vfsStream::newFile('upload.txt')->
			at($this->vfs)->
			setContent('Hello, World!');

		$this->destination = vfsStream::newDirectory('uploaded')->at($this->vfs);
	}

	/**
	 * @covers ::fromPhpFile
	 * @covers ::__construct
	 * @covers ::getSize
	 * @covers ::getError
	 * @covers ::getClientFilename
	 * @covers ::getClientMediaType
	 */
	public function testFromPhpFile()
	{
		$file = UploadedFile::fromPhpFile([
			'name' => $this->upload->getName(),
			'tmp_name' => $this->upload->url(),
			'type' => 'application/unknown',
			'error' => UPLOAD_ERR_OK,
			'size' => 1000
		]);

		$this->assertTrue($file instanceof UploadedFile);
		$this->assertSame($this->upload->getName(), $file->getClientFilename());
		$this->assertSame('application/unknown', $file->getClientMediaType());
		$this->assertSame(UPLOAD_ERR_OK, $file->getError());
		$this->assertSame(1000, $file->getSize());
	}

	/**
	 * @covers ::fromArray
	 * @covers ::__construct
	 * @covers ::getSize
	 * @covers ::getError
	 * @covers ::getClientFilename
	 * @covers ::getClientMediaType
	 */
	public function testFromArray()
	{
		$file = UploadedFile::fromArray([
			'clientFilename' => $this->upload->getName(),
			'localPath' => $this->upload->url(),
			'clientMediaType' => 'application/unknown',
			'error' => UPLOAD_ERR_OK,
			'size' => 1000
		]);

		$this->assertTrue($file instanceof UploadedFile);
		$this->assertSame($this->upload->getName(), $file->getClientFilename());
		$this->assertSame('application/unknown', $file->getClientMediaType());
		$this->assertSame(UPLOAD_ERR_OK, $file->getError());
		$this->assertSame(1000, $file->getSize());
	}

	/**
	 * @covers ::getSize
	 */
	public function testGetAutodetectedSize()
	{
		$file = new UploadedFile($this->upload->url());

		$this->assertSame(filesize($this->upload->url()), $file->getSize());
	}

	/**
	 * @covers ::getSize
	 */
	public function testGetExplicitSize()
	{
		$file = UploadedFile::fromArray([
			'localPath' => $this->upload->url(),
			'size' => 42
		]);

		$this->assertTrue($file instanceof UploadedFile);
		$this->assertSame(42, $file->getSize());
	}

	/**
	 * @covers ::getStream
	 */
	public function testGetStream()
	{
		$file = new UploadedFile($this->upload->url());

		$this->assertTrue($file->getStream() instanceof StreamInterface);
		$this->assertSame(file_get_contents($this->upload->url()), (string) $file->getStream());
	}

	/**
	 * @covers ::getStream
	 * @covers ::moveTo
	 *
	 * @expectedException \RuntimeException
	 */
	public function testGetStreamAfterMove()
	{
		$file = new UploadedFile($this->upload->url());
		$file->moveTo($this->destination->url());
		$file->getStream();
	}

	/**
	 * @covers ::moveTo
	 *
	 * @expectedException \RuntimeException
	 */
	public function testMoveToInvalidPath()
	{
		$file = new UploadedFile($this->upload->url());
		@$file->moveTo('vfs://invalid');
	}
}