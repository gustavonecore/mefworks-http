<?php namespace mef\Http;

use InvalidArgumentException;
use Psr\Http\Message\UriInterface;

class Uri implements UriInterface
{
	/**
	 * @var string
	 */
	private $scheme = '';

	/**
	 * @var string
	 */
	private $user = '';

	/**
	 * @var string
	 */
	private $password = '';

	/**
	 * @var int|null
	 */
	private $port;

	/**
	 * @var string
	 */
	private $host = '';

	/**
	 * @var string
	 */
	private $path = '';

	/**
	 * @var string
	 */
	private $query = '';

	/**
	 * @var string
	 */
	private $fragment = '';

	/**
	 * A hardcoded list of default ports. If a Uri uses the default
	 * port then it will be omitted from various calls like getPort()
	 *
	 * @var array
	 */
	private static $defaultPorts = [
		'telnet' => 20,
		'ftp' => 21,
		'telnet' => 22,
		'ssh' => 23,
		'http' => 80,
		'https' => 443
	];

	/**
	 * Parse the string and return a URI.
	 *
	 * @param  string $string A well formed Uri string
	 *
	 * @return \mef\Http\Uri
	 */
	public static function fromString($string)
	{
		$components = parse_url($string);

		if (is_array($components) === false)
		{
			throw new InvalidArgumentException('$string is not a valid URI');
		}

		$components += [
			'scheme' => '',
			'host' => '',
			'port' => null,
			'user' => '',
			'pass' => '',
			'path' => '',
			'query' => '',
			'fragment' => ''
		];

		return new self(
			$components['scheme'],
			$components['user'],
			$components['pass'],
			$components['host'],
			$components['port'],
			$components['path'],
			$components['query'],
			$components['fragment']
		);
	}

	/**
	 * Constructor
	 *
	 * @param string   $scheme   The scheme (e.g., http)
	 * @param string   $user     The user name
	 * @param string   $password The password
	 * @param string   $host     The host (domain, IP Address, etc)
	 * @param int|null $port     The port
	 * @param string   $path     The path
	 * @param string   $query    The query string (e.g., foo=bar&x=1)
	 * @param string   $fragment The fragment (without hash)
	 */
	public function __construct($scheme = '', $user = '', $password = '', $host = '', $port = null, $path = '', $query = '', $fragment = '')
	{
		$this->setScheme($scheme);
		$this->user = $user;
		$this->password = $password;
		$this->host = $host;
		$this->port = $port;
		$this->path = $path;
		$this->query = $query;
		$this->fragment = $fragment;
	}

	/**
	 * Retrieve the scheme component of the URI.
	 *
	 * If no scheme is present, this method MUST return an empty string.
	 *
	 * The value returned MUST be normalized to lowercase, per RFC 3986
	 * Section 3.1.
	 *
	 * The trailing ":" character is not part of the scheme and MUST NOT be
	 * added.
	 *
	 * @see https://tools.ietf.org/html/rfc3986#section-3.1
	 * @return string The URI scheme.
	 */
	public function getScheme()
	{
		return $this->scheme;
	}

	/**
	 * Retrieve the authority component of the URI.
	 *
	 * If no authority information is present, this method MUST return an empty
	 * string.
	 *
	 * The authority syntax of the URI is:
	 *
	 * <pre>
	 * [user-info@]host[:port]
	 * </pre>
	 *
	 * If the port component is not set or is the standard port for the current
	 * scheme, it SHOULD NOT be included.
	 *
	 * @see https://tools.ietf.org/html/rfc3986#section-3.2
	 * @return string The URI authority, in "[user-info@]host[:port]" format.
	 */
	public function getAuthority()
	{
		$authority = $this->getUserInfo();

		if ($authority !== '')
		{
			$authority .= '@';
		}

		$authority .= $this->host;

		if ($this->port !== null && (!isset(self::$defaultPorts[$this->scheme]) || self::$defaultPorts[$this->scheme] !== $this->port))
		{
			$authority .= ':' . $this->port;
		}

		return $authority;
	}

	/**
	 * Retrieve the user information component of the URI.
	 *
	 * If no user information is present, this method MUST return an empty
	 * string.
	 *
	 * If a user is present in the URI, this will return that value;
	 * additionally, if the password is also present, it will be appended to the
	 * user value, with a colon (":") separating the values.
	 *
	 * The trailing "@" character is not part of the user information and MUST
	 * NOT be added.
	 *
	 * @return string The URI user information, in "username[:password]" format.
	 */
	public function getUserInfo()
	{
		$userInfo = '';

		if ($this->user !== '')
		{
			$userInfo .= $this->user;

			if ($this->password !== '')
			{
				$userInfo .= ':' . $this->password;
			}
		}

		return $userInfo;
	}

	/**
	 * Retrieve the host component of the URI.
	 *
	 * If no host is present, this method MUST return an empty string.
	 *
	 * The value returned MUST be normalized to lowercase, per RFC 3986
	 * Section 3.2.2.
	 *
	 * @see http://tools.ietf.org/html/rfc3986#section-3.2.2
	 * @return string The URI host.
	 */
	public function getHost()
	{
		return $this->host;
	}

	/**
	 * Retrieve the port component of the URI.
	 *
	 * If a port is present, and it is non-standard for the current scheme,
	 * this method MUST return it as an integer. If the port is the standard port
	 * used with the current scheme, this method SHOULD return null.
	 *
	 * If no port is present, and no scheme is present, this method MUST return
	 * a null value.
	 *
	 * If no port is present, but a scheme is present, this method MAY return
	 * the standard port for that scheme, but SHOULD return null.
	 *
	 * @return null|int The URI port.
	 */
	public function getPort()
	{
		if (isset(self::$defaultPorts[$this->scheme]) === false || self::$defaultPorts[$this->scheme] !== $this->port)
		{
			return $this->port;
		}
		else
		{
			return null;
		}
	}

	/**
	 * Retrieve the path component of the URI.
	 *
	 * The path can either be empty or absolute (starting with a slash) or
	 * rootless (not starting with a slash). Implementations MUST support all
	 * three syntaxes.
	 *
	 * Normally, the empty path "" and absolute path "/" are considered equal as
	 * defined in RFC 7230 Section 2.7.3. But this method MUST NOT automatically
	 * do this normalization because in contexts with a trimmed base path, e.g.
	 * the front controller, this difference becomes significant. It's the task
	 * of the user to handle both "" and "/".
	 *
	 * The value returned MUST be percent-encoded, but MUST NOT double-encode
	 * any characters. To determine what characters to encode, please refer to
	 * RFC 3986, Sections 2 and 3.3.
	 *
	 * As an example, if the value should include a slash ("/") not intended as
	 * delimiter between path segments, that value MUST be passed in encoded
	 * form (e.g., "%2F") to the instance.
	 *
	 * @see https://tools.ietf.org/html/rfc3986#section-2
	 * @see https://tools.ietf.org/html/rfc3986#section-3.3
	 * @return string The URI path.
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 * Retrieve the query string of the URI.
	 *
	 * If no query string is present, this method MUST return an empty string.
	 *
	 * The leading "?" character is not part of the query and MUST NOT be
	 * added.
	 *
	 * The value returned MUST be percent-encoded, but MUST NOT double-encode
	 * any characters. To determine what characters to encode, please refer to
	 * RFC 3986, Sections 2 and 3.4.
	 *
	 * As an example, if a value in a key/value pair of the query string should
	 * include an ampersand ("&") not intended as a delimiter between values,
	 * that value MUST be passed in encoded form (e.g., "%26") to the instance.
	 *
	 * @see https://tools.ietf.org/html/rfc3986#section-2
	 * @see https://tools.ietf.org/html/rfc3986#section-3.4
	 * @return string The URI query string.
	 */
	public function getQuery()
	{
		return $this->query;
	}

	/**
	 * Retrieve the fragment component of the URI.
	 *
	 * If no fragment is present, this method MUST return an empty string.
	 *
	 * The leading "#" character is not part of the fragment and MUST NOT be
	 * added.
	 *
	 * The value returned MUST be percent-encoded, but MUST NOT double-encode
	 * any characters. To determine what characters to encode, please refer to
	 * RFC 3986, Sections 2 and 3.5.
	 *
	 * @see https://tools.ietf.org/html/rfc3986#section-2
	 * @see https://tools.ietf.org/html/rfc3986#section-3.5
	 * @return string The URI fragment.
	 */
	public function getFragment()
	{
		return $this->fragment;
	}

	/**
	 * Return an instance with the specified scheme.
	 *
	 * This method MUST retain the state of the current instance, and return
	 * an instance that contains the specified scheme.
	 *
	 * Implementations MUST support the schemes "http" and "https" case
	 * insensitively, and MAY accommodate other schemes if required.
	 *
	 * An empty scheme is equivalent to removing the scheme.
	 *
	 * @param string $scheme The scheme to use with the new instance.
	 * @return self A new instance with the specified scheme.
	 * @throws \InvalidArgumentException for invalid or unsupported schemes.
	 */
	public function withScheme($scheme)
	{
		$scheme = strtolower($scheme);

		if ($scheme === $this->scheme)
		{
			return $this;
		}

		$uri = clone $this;
		$uri->setScheme($scheme);

		return $uri;
	}

	/**
	 * Return an instance with the specified user information.
	 *
	 * This method MUST retain the state of the current instance, and return
	 * an instance that contains the specified user information.
	 *
	 * Password is optional, but the user information MUST include the
	 * user; an empty string for the user is equivalent to removing user
	 * information.
	 *
	 * @param string $user The user name to use for authority.
	 * @param null|string $password The password associated with $user.
	 * @return self A new instance with the specified user information.
	 */
	public function withUserInfo($user, $password = null)
	{
		$user = (string) $user;
		$password = (string) $password;

		if ($this->user === $user && $this->password === $password)
		{
			return $this;
		}

		$uri = clone $this;
		$uri->user = $user;
		$uri->password = $password;

		return $uri;
	}

	/**
	 * Return an instance with the specified host.
	 *
	 * This method MUST retain the state of the current instance, and return
	 * an instance that contains the specified host.
	 *
	 * An empty host value is equivalent to removing the host.
	 *
	 * @param string $host The hostname to use with the new instance.
	 * @return self A new instance with the specified host.
	 * @throws \InvalidArgumentException for invalid hostnames.
	 */
	public function withHost($host)
	{
		$host = (string) $host;

		if ($host === $this->host)
		{
			return $this;
		}

		$uri = clone $this;
		$uri->host = $host;

		return $uri;
	}

	/**
	 * Return an instance with the specified port.
	 *
	 * This method MUST retain the state of the current instance, and return
	 * an instance that contains the specified port.
	 *
	 * Implementations MUST raise an exception for ports outside the
	 * established TCP and UDP port ranges.
	 *
	 * A null value provided for the port is equivalent to removing the port
	 * information.
	 *
	 * @param null|int $port The port to use with the new instance; a null value
	 *     removes the port information.
	 * @return self A new instance with the specified port.
	 * @throws \InvalidArgumentException for invalid ports.
	 */
	public function withPort($port)
	{
		if ($this->port === $port)
		{
			return $this;
		}

		$uri = clone $this;
		$uri->port = $port;
		return $uri;
	}

	/**
	 * Return an instance with the specified path.
	 *
	 * This method MUST retain the state of the current instance, and return
	 * an instance that contains the specified path.
	 *
	 * The path can either be empty or absolute (starting with a slash) or
	 * rootless (not starting with a slash). Implementations MUST support all
	 * three syntaxes.
	 *
	 * If the path is intended to be domain-relative rather than path relative then
	 * it must begin with a slash ("/"). Paths not starting with a slash ("/")
	 * are assumed to be relative to some base path known to the application or
	 * consumer.
	 *
	 * Users can provide both encoded and decoded path characters.
	 * Implementations ensure the correct encoding as outlined in getPath().
	 *
	 * @param string $path The path to use with the new instance.
	 * @return self A new instance with the specified path.
	 * @throws \InvalidArgumentException for invalid paths.
	 */
	public function withPath($path)
	{
		$path = (string) $path;

		if ($path === $this->path)
		{
			return $this;
		}

		$uri = clone $this;
		$uri->path = $path;

		return $uri;
	}

	/**
	 * Return an instance with the specified query string.
	 *
	 * This method MUST retain the state of the current instance, and return
	 * an instance that contains the specified query string.
	 *
	 * Users can provide both encoded and decoded query characters.
	 * Implementations ensure the correct encoding as outlined in getQuery().
	 *
	 * An empty query string value is equivalent to removing the query string.
	 *
	 * @param string $query The query string to use with the new instance.
	 * @return self A new instance with the specified query string.
	 * @throws \InvalidArgumentException for invalid query strings.
	 */
	public function withQuery($query)
	{
		$query = (string) $query;

		if ($query === $this->query)
		{
			return $this;
		}

		$uri = clone $this;
		$uri->query = $query;

		return $uri;
	}

	/**
	 * Return an instance with the specified URI fragment.
	 *
	 * This method MUST retain the state of the current instance, and return
	 * an instance that contains the specified URI fragment.
	 *
	 * Users can provide both encoded and decoded fragment characters.
	 * Implementations ensure the correct encoding as outlined in getFragment().
	 *
	 * An empty fragment value is equivalent to removing the fragment.
	 *
	 * @param string $fragment The fragment to use with the new instance.
	 * @return self A new instance with the specified fragment.
	 */
	public function withFragment($fragment)
	{
		$fragment = (string) $fragment;

		if ($fragment === $this->fragment)
		{
			return $this;
		}

		$uri = clone $this;
		$uri->fragment = $fragment;

		return $uri;
	}

	/**
	 * Return the string representation as a URI reference.
	 *
	 * Depending on which components of the URI are present, the resulting
	 * string is either a full URI or relative reference according to RFC 3986,
	 * Section 4.1. The method concatenates the various components of the URI,
	 * using the appropriate delimiters:
	 *
	 * - If a scheme is present, it MUST be suffixed by ":".
	 * - If an authority is present, it MUST be prefixed by "//".
	 * - The path can be concatenated without delimiters. But there are two
	 *   cases where the path has to be adjusted to make the URI reference
	 *   valid as PHP does not allow to throw an exception in __toString():
	 *     - If the path is rootless and an authority is present, the path MUST
	 *       be prefixed by "/".
	 *     - If the path is starting with more than one "/" and no authority is
	 *       present, the starting slashes MUST be reduced to one.
	 * - If a query is present, it MUST be prefixed by "?".
	 * - If a fragment is present, it MUST be prefixed by "#".
	 *
	 * @see http://tools.ietf.org/html/rfc3986#section-4.1
	 * @return string
	 */
	public function __toString()
	{
		$uri = '';

		if ($this->scheme  !== '')
		{
			$uri .= $this->scheme . ':';
		}

		$authority = $this->getAuthority();
		if ($authority !== '')
		{
			$uri .= '//' . $authority;
		}

		if ($this->path !== '')
		{
			if ($authority !== '' && substr($this->path, 0, 1) !== '/')
			{
				$uri .= '/' . $this->path;
			}
			else if ($authority === '')
			{
				$uri .= '/' . ltrim($this->path, '/');
			}
			else
			{
				$uri .= $this->path;
			}
		}

		if ($this->query !== '')
		{
			$uri .= '?' . $this->query;
		}

		if ($this->fragment !== '')
		{
			$uri .= '#' . $this->fragment;
		}

		return $uri;
	}


	/**
	 * Change the scheme.
	 *
	 * It must begin with a letter and be followed by zero or more
	 * letters, numbers, +, -, or . characters.
	 *
	 * It will be normalized to lowercase.
	 *
	 * @param string $scheme
	 */
	private function setScheme($scheme)
	{
		$scheme = (string) $scheme;

		if ($scheme !== '' && !preg_match('/^[a-z][a-z0-9+\.-]*$/i', $scheme))
		{
			throw new InvalidArgumentException('scheme ' . $scheme . ' is invalid. It must begin with a letter and only contain letters, numbers, and the characters: .+-');
		}

		$this->scheme = strtolower($scheme);
	}
}
