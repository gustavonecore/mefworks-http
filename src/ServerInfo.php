<?php namespace mef\Http;

/**
 * Wraps the $_SERVER variable.
 */
class ServerInfo
{
	/**
	 * The $_SERVER variable, or any array with the same format.
	 *
	 * @var array
	 */
	private $server;

	/**
	 * Constructor
	 *
	 * @param  array $server  The $_SERVER variable, or any array with the
	 *                        same format.
	 */
	public function __construct(array $server)
	{
		$this->server = $server;
	}

	/**
	 * Return the HTTP method.
	 *
	 * @return string
	 */
	public function getHttpMethod()
	{
		return isset($this->server['REQUEST_METHOD']) ? $this->server['REQUEST_METHOD'] : '';
	}

	/**
	 * Return the URI.
	 *
	 * @return \mef\Http\Uri
	 */
	public function getUri()
	{
		$uri = isset($this->server['HTTPS']) && $this->server['HTTPS'] === 'on' ? 'https:' : 'http:';

		if (isset($this->server['HTTP_HOST']) === true)
		{
			$uri .= '//' . $this->server['HTTP_HOST'];
		}

		if (isset($this->server['REQUEST_URI']) === true)
		{
			$uri .= $this->server['REQUEST_URI'];
		}

		return Uri::fromString($uri);
	}

	/**
	 * Return the HTTP headers.
	 *
	 * @return array
	 */
	public function getHttpHeaders()
	{
		$headers = [];

		if (isset($this->server['CONTENT_TYPE']) === true)
		{
			$headers['CONTENT-TYPE'] = $this->server['CONTENT_TYPE'];
		}

		foreach ($this->server as $key => $value)
		{
			if (substr($key, 0, 5) === 'HTTP_' && $key !== 'HTTP_')
			{
				$headers[str_replace('_', '-', substr($key, 5))] = $value;
			}
		}

		return $headers;
	}

	/**
	 * Return the HTTP protocol version.
	 *
	 * @return string
	 */
	public function getProtocolVersion()
	{
		if (isset($this->server['SERVER_PROTOCOL']) === true &&
			preg_match('#^HTTP/(\d+\.\d+)$#', $this->server['SERVER_PROTOCOL'], $m))
		{
			return $m[1];
		}
		else
		{
			return '';
		}
	}
}