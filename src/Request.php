<?php namespace mef\Http;

use InvalidArgumentException;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;

class Request extends Message implements RequestInterface
{
	/**
	 * @var string
	 */
	private $requestTarget;

	/**
	 * @var \Psr\Http\Message\UriInterface
	 */
	private $uri;

	/**
	 * @var string
	 */
	private $method;

	/**
	 * Filter the given array of properties.
	 *
	 * @param array $properties
	 *
	 * @return array
	 */
	private static function filterArray(array $properties)
	{
		$properties += [
			'method' => 'GET',
			'uri' => null,
			'requestTarget' => ''
		];

		if (is_string($properties['uri']) === true)
		{
			$properties['uri'] = Uri::fromString($properties['uri']);
		}

		return $properties;
	}

	/**
	 * Constructor
	 *
	 * @param array $properties An array with the following keys:
	 *   - body:            string|Psr\Http\Message\StreamInterface
	 *   - headers:         array   key/value headers
	 *   - protocolVersion: string  default '1.1'
	 *   - method:          string  default 'GET'
	 *   - uri:             string|\mef\Http\Uri
	 *   - requestTarget:   string
	 */
	public function __construct(array $properties)
	{
		parent::__construct($properties);

		$properties = self::filterArray($properties);

		$this->method = $properties['method'];
		$this->uri = $properties['uri'];
		$this->requestTarget = $properties['requestTarget'];
	}

	/**
	 * Retrieves the message's request target.
	 *
	 * Retrieves the message's request-target either as it will appear (for
	 * clients), as it appeared at request (for servers), or as it was
	 * specified for the instance (see withRequestTarget()).
	 *
	 * In most cases, this will be the origin-form of the composed URI,
	 * unless a value was provided to the concrete implementation (see
	 * withRequestTarget() below).
	 *
	 * If no URI is available, and no request-target has been specifically
	 * provided, this method MUST return the string "/".
	 *
	 * @return string
	 */
	public function getRequestTarget()
	{
		if ($this->requestTarget === '')
		{
			$requestTarget = $this->uri->getPath();

			if ($requestTarget === '')
			{
				$requestTarget = '/';
			}

			$query = $this->uri->getQuery();
			if ($query !== '')
			{
				$requestTarget .= '?' . $query;
			}

			return $requestTarget;
		}
		else
		{
			return $this->requestTarget;
		}
	}

	/**
	 * Return an instance with the specific request-target.
	 *
	 * If the request needs a non-origin-form request-target — e.g., for
	 * specifying an absolute-form, authority-form, or asterisk-form —
	 * this method may be used to create an instance with the specified
	 * request-target, verbatim.
	 *
	 * This method MUST be implemented in such a way as to retain the
	 * immutability of the message, and MUST return an instance that has the
	 * changed request target.
	 *
	 * @link http://tools.ietf.org/html/rfc7230#section-2.7 (for the various
	 *     request-target forms allowed in request messages)
	 * @param mixed $requestTarget
	 * @return self
	 */
	public function withRequestTarget($requestTarget)
	{
		$requestTarget = (string) $requestTarget;

		if ($requestTarget === $this->requestTarget)
		{
			return $this;
		}

		$request = clone $this;
		$request->requestTarget = $requestTarget;

		return $request;
	}

	/**
	 * Retrieves the HTTP method of the request.
	 *
	 * @return string Returns the request method.
	 */
	public function getMethod()
	{
		return $this->method;
	}

	/**
	 * Return an instance with the provided HTTP method.
	 *
	 * While HTTP method names are typically all uppercase characters, HTTP
	 * method names are case-sensitive and thus implementations SHOULD NOT
	 * modify the given string.
	 *
	 * This method MUST be implemented in such a way as to retain the
	 * immutability of the message, and MUST return an instance that has the
	 * changed request method.
	 *
	 * @param string $method Case-sensitive method.
	 * @return self
	 * @throws \InvalidArgumentException for invalid HTTP methods.
	 */
	public function withMethod($method)
	{
		$method = (string) $method;

		if ($method === $this->method)
		{
			return $this;
		}

		$request = clone $this;
		$request->method = $method;

		return $request;
	}

	/**
	 * Retrieves the URI instance.
	 *
	 * This method MUST return a UriInterface instance.
	 *
	 * @link http://tools.ietf.org/html/rfc3986#section-4.3
	 * @return UriInterface Returns a UriInterface instance
	 *     representing the URI of the request.
	 */
	public function getUri()
	{
		return $this->uri;
	}

	/**
	 * Returns an instance with the provided URI.
	 *
	 * This method MUST update the Host header of the returned request by
	 * default if the URI contains a host component. If the URI does not
	 * contain a host component, any pre-existing Host header MUST be carried
	 * over to the returned request.
	 *
	 * You can opt-in to preserving the original state of the Host header by
	 * setting `$preserveHost` to `true`. When `$preserveHost` is set to
	 * `true`, this method interacts with the Host header in the following ways:
	 *
	 * - If the the Host header is missing or empty, and the new URI contains
	 *   a host component, this method MUST update the Host header in the returned
	 *   request.
	 * - If the Host header is missing or empty, and the new URI does not contain a
	 *   host component, this method MUST NOT update the Host header in the returned
	 *   request.
	 * - If a Host header is present and non-empty, this method MUST NOT update
	 *   the Host header in the returned request.
	 *
	 * This method MUST be implemented in such a way as to retain the
	 * immutability of the message, and MUST return an instance that has the
	 * new UriInterface instance.
	 *
	 * @link http://tools.ietf.org/html/rfc3986#section-4.3
	 * @param UriInterface $uri New request URI to use.
	 * @param bool $preserveHost Preserve the original state of the Host header.
	 * @return self
	 */
	public function withUri(UriInterface $uri, $preserveHost = false)
	{
		$newHost = '';

		if ($uri->getHost() !== '' && ($preserveHost === false || $this->getHeaderLine('host') === ''))
		{
			$newHost = $uri->getHost();
		}

		if (($newHost === '' || $newHost === $this->getHeaderLine('Host')) &&
			(string) $uri === (string) $this->uri)
		{
			return $this;
		}

		$request = clone $this;
		$request->uri = $uri;

		if ($newHost !== '')
		{
			$request = $request->withHeader('host', $newHost);
		}

		return $request;
	}
}
