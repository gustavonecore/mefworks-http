<?php namespace mef\Http\Test\Unit;

use mef\Http\Response;

/**
 * @coversDefaultClass \mef\Http\Response
 */
class ResponseTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::filterArray
	 * @covers ::__construct
	 * @covers ::getStatusCode
	 * @covers ::getReasonPhrase
	 */
	public function testFromArray()
	{
		$response = new Response([
			'body' => 'body',
			'headers' => ['Set-Cookie' => 'foo=bar'],
			'code' => 404,
			'reasonPhrase' => 'File Not Found',
			'protocolVersion' => '1.2'
		]);

		$this->assertTrue($response instanceof Response);
		$this->assertSame('body', (string) $response->getBody());
		$this->assertSame('foo=bar', $response->getHeaderLine('Set-Cookie'));
		$this->assertSame(404, $response->getStatusCode());
		$this->assertSame('File Not Found', $response->getReasonPhrase());
		$this->assertSame('1.2', $response->getProtocolVersion());
	}

	/**
	 * @covers ::filterArray
	 * @covers ::__construct
	 * @covers ::getStatusCode
	 * @covers ::getReasonPhrase
	 */
	public function testFromEmptyArray()
	{
		$response = new Response([]);

		$this->assertTrue($response instanceof Response);
		$this->assertSame('', (string) $response->getBody());
		$this->assertSame([], $response->getHeaders());
		$this->assertSame(200, $response->getStatusCode());
		$this->assertNotSame('', $response->getReasonPhrase());
		$this->assertSame('1.1', $response->getProtocolVersion());
	}

	/**
	 * @covers ::withStatus
	 * @covers ::setStatus
	 * @covers ::getStatusCode
	 * @covers ::getReasonPhrase
	 */
	public function testWithStatus()
	{
		$response = new Response([]);
		$this->assertTrue($response instanceof Response);

		$response2 = $response->withStatus(200, 'Foo');

		$this->assertTrue($response2 instanceof Response);
		$this->assertNotSame($response, $response2);
		$this->assertSame(200, $response2->getStatusCode());
		$this->assertSame('Foo', $response2->getReasonPhrase());
	}

	/**
	 * @covers ::withStatus
	 * @covers ::setStatus
	 * @covers ::getStatusCode
	 * @covers ::getReasonPhrase
	 */
	public function testWithSameStatus()
	{
		$response = new Response(['code' => 200, 'reasonPhrase' => 'Foo']);
		$this->assertTrue($response instanceof Response);

		$response2 = $response->withStatus(200, 'Foo');

		$this->assertTrue($response2 instanceof Response);
		$this->assertSame($response, $response2);
		$this->assertSame(200, $response2->getStatusCode());
		$this->assertSame('Foo', $response2->getReasonPhrase());
	}

	/**
	 * Test that the known status code of 200 returns a non-empty phrase.
	 * @covers ::getReasonPhrase
	 */
	public function testDefaultReasonPhrase()
	{
		$response = new Response(['code' => 200]);
		$this->assertTrue($response instanceof Response);

		$this->assertTrue(is_string($response->getReasonPhrase()));
	}

	/**
	 * @covers ::setStatus
	 * @expectedException \InvalidArgumentException
	 */
	public function testInvalidStatusCode99()
	{
		new Response(['code' => 99]);
	}

	/**
	 * @covers ::setStatus
	 * @expectedException \InvalidArgumentException
	 */
	public function testInvalidStatusCode1000()
	{
		new Response(['code' => 1000]);
	}

	/**
	 * @covers ::setStatus
	 * @covers ::getStatusCode
	 */
	public function testValidStatusCode100()
	{
		$response = new Response(['code' => 100]);
		$this->assertTrue($response instanceof Response);
		$this->assertSame(100, $response->getStatusCode());
	}

	/**
	 * @covers ::setStatus
	 * @covers ::getStatusCode
	 */
	public function testValidStatusCode999()
	{
		$response = new Response(['code' => 999]);
		$this->assertTrue($response instanceof Response);
		$this->assertSame(999, $response->getStatusCode());
	}
}