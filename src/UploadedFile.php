<?php namespace mef\Http;

use RuntimeException;
use Psr\Http\Message\UploadedFileInterface;

/**
 * Value object representing a file uploaded through an HTTP request.
 *
 * Instances of this interface are considered immutable; all methods that
 * might change state MUST be implemented such that they retain the internal
 * state of the current instance and return an instance that contains the
 * changed state.
 */
class UploadedFile implements UploadedFileInterface
{
	/**
	 * @var string
	 */
	private $localPath;

	/**
	 * @var boolean
	 */
	private $isMoved = false;

	/**
	 * @var int
	 */
	private $error = UPLOAD_ERR_OK;

	/**
	 * @var string
	 */
	private $clientFilename;

	/**
	 * @var string
	 */
	private $clientMediaType;

	/**
	 * @var int|null
	 */
	private $size;

	/**
	 * Create a new instance based off of information from the PHP $_FILES
	 * array.
	 *
	 * - string   name
	 * - string   tmp_name
	 * - string   type
	 * - int      error
	 * - int|null size
	 *
	 * @param  array  $file
	 *
	 * @return \mef\Http\UploadedFile
	 */
	public static function fromPhpFile(array $file)
	{
		$file += [
			'name' => '',
			'tmp_name' => '',
			'type' => '',
			'error' => UPLOAD_ERR_OK,
			'size' => null
		];

		return new self($file['tmp_name'], $file['name'], $file['type'], $file['error'], $file['size']);
	}

	/**
	 * Create a new instance based off of information from the array.
	 *
	 * - string   clientFilename
	 * - string   localPath
	 * - string   clientMediaType
	 * - int      error
	 * - int|null size
	 *
	 * @param  array  $file
	 *
	 * @return \mef\Http\UploadedFile
	 */
	public static function fromArray(array $file)
	{
		$file += [
			'clientFilename' => '',
			'localPath' => '',
			'clientMediaType' => '',
			'error' => UPLOAD_ERR_OK,
			'size' => null
		];

		return new self($file['localPath'], $file['clientFilename'], $file['clientMediaType'], $file['error'], $file['size']);
	}

	/**
	 * Constructor
	 *
	 * @param string   $localPath The temporary, local path to the uploaded file
	 * @param string   $filename  The filename as reported by the client
	 * @param string   $mediaType The mime type as reported by the client
	 * @param int      $error     One of PHP's UPLOAD_ERR_* constants
	 * @param int|null $size      The size of the file in bytes, or null if unknown
	 */
	public function __construct($localPath, $filename = '', $mediaType = '', $error = UPLOAD_ERR_OK, $size = null)
	{
		$this->localPath = (string) $localPath;
		$this->clientFilename = (string) $filename;
		$this->clientMediaType = (string) $mediaType;
		$this->error = (int) $error;
		$this->size = $size === null ? null : (int) $size;
	}

	/**
	 * Retrieve a stream representing the uploaded file.
	 *
	 * This method MUST return a StreamInterface instance, representing the
	 * uploaded file. The purpose of this method is to allow utilizing native PHP
	 * stream functionality to manipulate the file upload, such as
	 * stream_copy_to_stream() (though the result will need to be decorated in a
	 * native PHP stream wrapper to work with such functions).
	 *
	 * If the moveTo() method has been called previously, this method MUST raise
	 * an exception.
	 *
	 * @return StreamInterface Stream representation of the uploaded file.
	 * @throws \RuntimeException in cases when no stream is available or can be
	 *     created.
	 */
	public function getStream()
	{
		if ($this->isMoved === true)
		{
			throw new RuntimeException('The file has already been moved.');
		}

		return new FileStream(fopen($this->localPath, 'r'));
	}

	/**
	 * Move the uploaded file to a new location.
	 *
	 * Use this method as an alternative to move_uploaded_file(). This method is
	 * guaranteed to work in both SAPI and non-SAPI environments.
	 * Implementations must determine which environment they are in, and use the
	 * appropriate method (move_uploaded_file(), rename(), or a stream
	 * operation) to perform the operation.
	 *
	 * $targetPath may be an absolute path, or a relative path. If it is a
	 * relative path, resolution should be the same as used by PHP's rename()
	 * function.
	 *
	 * The original file or stream MUST be removed on completion.
	 *
	 * If this method is called more than once, any subsequent calls MUST raise
	 * an exception.
	 *
	 * When used in an SAPI environment where $_FILES is populated, when writing
	 * files via moveTo(), is_uploaded_file() and move_uploaded_file() SHOULD be
	 * used to ensure permissions and upload status are verified correctly.
	 *
	 * If you wish to move to a stream, use getStream(), as SAPI operations
	 * cannot guarantee writing to stream destinations.
	 *
	 * @see http://php.net/is_uploaded_file
	 * @see http://php.net/move_uploaded_file
	 * @param string $targetPath Path to which to move the uploaded file.
	 * @throws \InvalidArgumentException if the $path specified is invalid.
	 * @throws \RuntimeException on any error during the move operation, or on
	 *     the second or subsequent call to the method.
	 */
	public function moveTo($targetPath)
	{
		if (rename($this->localPath, $targetPath) === false)
		{
			throw new RuntimeException('Unable to move file');
		}

		$this->isMoved = true;
	}

	/**
	 * Retrieve the file size.
	 *
	 * Implementations SHOULD return the value stored in the "size" key of
	 * the file in the $_FILES array if available, as PHP calculates this based
	 * on the actual size transmitted.
	 *
	 * @return int|null The file size in bytes or null if unknown.
	 */
	public function getSize()
	{
		if ($this->size === null)
		{
			$this->size = filesize($this->localPath);
		}

		return $this->size === false ? null : $this->size;
	}

	/**
	 * Retrieve the error associated with the uploaded file.
	 *
	 * The return value MUST be one of PHP's UPLOAD_ERR_XXX constants.
	 *
	 * If the file was uploaded successfully, this method MUST return
	 * UPLOAD_ERR_OK.
	 *
	 * Implementations SHOULD return the value stored in the "error" key of
	 * the file in the $_FILES array.
	 *
	 * @see http://php.net/manual/en/features.file-upload.errors.php
	 * @return int One of PHP's UPLOAD_ERR_XXX constants.
	 */
	public function getError()
	{
		return $this->error;
	}

	/**
	 * Retrieve the filename sent by the client.
	 *
	 * Do not trust the value returned by this method. A client could send
	 * a malicious filename with the intention to corrupt or hack your
	 * application.
	 *
	 * Implementations SHOULD return the value stored in the "name" key of
	 * the file in the $_FILES array.
	 *
	 * @return string|null The filename sent by the client or null if none
	 *     was provided.
	 */
	public function getClientFilename()
	{
		return $this->clientFilename;
	}

	/**
	 * Retrieve the media type sent by the client.
	 *
	 * Do not trust the value returned by this method. A client could send
	 * a malicious media type with the intention to corrupt or hack your
	 * application.
	 *
	 * Implementations SHOULD return the value stored in the "type" key of
	 * the file in the $_FILES array.
	 *
	 * @return string|null The media type sent by the client or null if none
	 *     was provided.
	 */
	public function getClientMediaType()
	{
		return $this->clientMediaType;
	}
}
