<?php namespace mef\Http;

use RuntimeException;
use InvalidArgumentException;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UploadedFileInterface;

class ServerRequest extends Request implements ServerRequestInterface
{
	/**
	 * A $_SERVER clone.
	 *
	 * @var array
	 */
	private $serverParams = [];

	/**
	 * A $_COOKIES clone.
	 *
	 * @var array
	 */
	private $cookieParams = [];

	/**
	 * A $_GET clone.
	 *
	 * @var array
	 */
	private $queryParams = [];

	/**
	 * An array of \mef\Http\UploadedFile objects.
	 *
	 * @var array
	 */
	private $uploadedFiles = [];

	/**
	 * Parsed body content, such as $_POST.
	 *
	 * @var null|array|object
	 */
	private $parsedBody;

	/**
	 * Arbitrary metadata.
	 *
	 * @var array
	 */
	private $attributes = [];

	/**
	 * Constructor
	 *
	 * @param array $properties An array with the following keys:
	 *   - body:            string|Psr\Http\Message\StreamInterface
	 *   - headers:         array   key/value headers
	 *   - protocolVersion: string  default '1.1'
	 *   - method:          string  default 'GET'
	 *   - uri:             string|\mef\Http\Uri
	 *   - requestTarget:   string
	 *   - serverParams:    array
	 *   - cookieParams:    array
	 *   - queryParams:     array
	 *   - uploadedFiles:   array
	 *   - parsedBody:      null|array|object
	 *   - attributes:      array
	 */
	public function __construct(array $properties = [])
	{
		parent::__construct($properties);

		$properties = self::filterArray($properties);

		$this->serverParams = $properties['serverParams'];
		$this->cookieParams = $properties['cookieParams'];
		$this->queryParams = $properties['queryParams'];
		$this->uploadedFiles = $properties['uploadedFiles'];
		$this->parsedBody = $properties['parsedBody'];
		$this->attributes = $properties['attributes'];
	}

	/**
	 * Filter the given array of properties.
	 *
	 * @param array $properties
	 *
	 * @return array
	 */
	protected static function filterArray(array $properties)
	{
		$properties += [
			'serverParams' => [],
			'cookieParams' => [],
			'queryParams' => [],
			'uploadedFiles' => [],
			'parsedBody' => null,
			'attributes' => []
		];

		return $properties;
	}

	/**
	 * Build a ServerRequest from the standard PHP superglobals.
	 *
	 * Often called like:
	 *
	 * <code>
	 * $request = \mef\Http\ServerRequest::fromGlobals(
	 *   new FileStream(fopen('php://input', 'r')),
	 *   $_SERVER,
	 *   $_GET,
	 *   $_POST,
	 *   $_COOKIE,
	 *   $_FILES
	 * );
	 * </code>
	 *
	 * @param \Psr\Http\Message\StreamInterface $body
	 * @param array $server   The $_SERVER global, or any array with the same
	 *                        format.
	 * @param array $get      The $_GET global, or any array with the same
	 *                        format.
	 * @param array $post     The $_POST global, or any array with the same
	 *                        format.
	 * @param array $cookies  The $_COOKIES global, or any array with the same
	 *                        format.
	 * @param array $files    The $_FILES global, or any array with the same
	 *                        format.
	 *
	 * @return  \mef\Http\ServerRequest
	 */
	public static function fromGlobals(StreamInterface $body, array $server = [], array $get = [], array $post = [], array $cookies = [], array $files = [])
	{
		$serverInfo = new ServerInfo($server);

		$serverRequest = new self([
			'method' => $serverInfo->getHttpMethod() ?: 'GET',
			'uri' => $serverInfo->getUri(),
			'body' => $body,
			'headers' => $serverInfo->getHttpHeaders(),
			'protocolVersion' => $serverInfo->getProtocolVersion() ?: '1.1',
			'serverParams' => $server,
			'cookieParams' => $cookies,
			'queryParams' => $get,
			'uploadedFiles' => (new PhpFiles($files))->getUploadedFiles()
		]);

		$decodedBody = $serverRequest->decodeBody();

		if ($decodedBody !== null)
		{
			if ($post !== [])
			{
				throw new RuntimeException('The POST data must be an empty array when the body contains encoded data.');
			}

			$serverRequest->parsedBody = $decodedBody;
		}
		else
		{
			$serverRequest->parsedBody = $post;
		}

		return $serverRequest;
	}

	/**
	 * Decode the body if it is encoded in a known format (JSON).
	 *
	 * If there is no encoded data, null is returned.
	 *
	 * @return array|null
	 */
	private function decodeBody()
	{
		$decodedBody = null;
		$contentType = explode(';', $this->getHeaderLine('Content-Type'))[0];

		if ($contentType === 'application/json')
		{
			$decodedBody = json_decode($this->getBody()->getContents(), true, 512, JSON_BIGINT_AS_STRING);

			if ($decodedBody === null)
			{
				if (json_last_error() !== JSON_ERROR_NONE)
				{
					throw new RuntimeException('The JSON is malformed.');
				}
			}

			if (is_array($decodedBody) === false)
			{
				throw new RuntimeException('The JSON must be an array or object.');
			}
		}

		return $decodedBody;
	}

	/**
	 * Retrieve server parameters.
	 *
	 * Retrieves data related to the incoming request environment,
	 * typically derived from PHP's $_SERVER superglobal. The data IS NOT
	 * REQUIRED to originate from $_SERVER.
	 *
	 * @return array
	 */
	public function getServerParams()
	{
		return $this->serverParams;
	}

	/**
	 * Retrieve cookies.
	 *
	 * Retrieves cookies sent by the client to the server.
	 *
	 * The data MUST be compatible with the structure of the $_COOKIE
	 * superglobal.
	 *
	 * @return array
	 */
	public function getCookieParams()
	{
		return $this->cookieParams;
	}

	/**
	 * Return an instance with the specified cookies.
	 *
	 * The data IS NOT REQUIRED to come from the $_COOKIE superglobal, but MUST
	 * be compatible with the structure of $_COOKIE. Typically, this data will
	 * be injected at instantiation.
	 *
	 * This method MUST NOT update the related Cookie header of the request
	 * instance, nor related values in the server params.
	 *
	 * This method MUST be implemented in such a way as to retain the
	 * immutability of the message, and MUST return an instance that has the
	 * updated cookie values.
	 *
	 * @param array $cookies Array of key/value pairs representing cookies.
	 * @return self
	 */
	public function withCookieParams(array $cookies)
	{
		if ($cookies === $this->cookieParams)
		{
			return $this;
		}

		$request = clone $this;
		$request->cookieParams = $cookies;

		return $request;
	}

	/**
	 * Retrieve query string arguments.
	 *
	 * Retrieves the deserialized query string arguments, if any.
	 *
	 * Note: the query params might not be in sync with the URI or server
	 * params. If you need to ensure you are only getting the original
	 * values, you may need to parse the query string from `getUri()->getQuery()`
	 * or from the `QUERY_STRING` server param.
	 *
	 * @return array
	 */
	public function getQueryParams()
	{
		return $this->queryParams;
	}

	/**
	 * Return an instance with the specified query string arguments.
	 *
	 * These values SHOULD remain immutable over the course of the incoming
	 * request. They MAY be injected during instantiation, such as from PHP's
	 * $_GET superglobal, or MAY be derived from some other value such as the
	 * URI. In cases where the arguments are parsed from the URI, the data
	 * MUST be compatible with what PHP's parse_str() would return for
	 * purposes of how duplicate query parameters are handled, and how nested
	 * sets are handled.
	 *
	 * Setting query string arguments MUST NOT change the URI stored by the
	 * request, nor the values in the server params.
	 *
	 * This method MUST be implemented in such a way as to retain the
	 * immutability of the message, and MUST return an instance that has the
	 * updated query string arguments.
	 *
	 * @param array $query Array of query string arguments, typically from
	 *     $_GET.
	 * @return self
	 */
	public function withQueryParams(array $query)
	{
		if ($query === $this->queryParams)
		{
			return $this;
		}

		$request = clone $this;
		$request->queryParams = $query;

		return $request;
	}

	/**
	 * Retrieve normalized file upload data.
	 *
	 * This method returns upload metadata in a normalized tree, with each leaf
	 * an instance of Psr\Http\Message\UploadedFileInterface.
	 *
	 * These values MAY be prepared from $_FILES or the message body during
	 * instantiation, or MAY be injected via withUploadedFiles().
	 *
	 * @return array An array tree of UploadedFileInterface instances; an empty
	 *     array MUST be returned if no data is present.
	 */
	public function getUploadedFiles()
	{
		return $this->uploadedFiles;
	}

	/**
	 * Create a new instance with the specified uploaded files.
	 *
	 * This method MUST be implemented in such a way as to retain the
	 * immutability of the message, and MUST return an instance that has the
	 * updated body parameters.
	 *
	 * @param array An array tree of UploadedFileInterface instances.
	 * @return self
	 * @throws \InvalidArgumentException if an invalid structure is provided.
	 */
	public function withUploadedFiles(array $uploadedFiles)
	{
		if ($this->uploadedFiles === $uploadedFiles)
		{
			return $this;
		}

		foreach ($uploadedFiles as $uploadedFile)
		{
			if ($uploadedFile instanceof UploadedFileInterface === false)
			{
				throw new InvalidArgumentException('Each uploaded file must implement ' . UploadedFileInterface::class);
			}
		}

		$request = clone $this;
		$request->uploadedFiles = $uploadedFiles;

		return $request;
	}

	/**
	 * Retrieve any parameters provided in the request body.
	 *
	 * If the request Content-Type is either application/x-www-form-urlencoded
	 * or multipart/form-data, and the request method is POST, this method MUST
	 * return the contents of $_POST.
	 *
	 * Otherwise, this method may return any results of deserializing
	 * the request body content; as parsing returns structured content, the
	 * potential types MUST be arrays or objects only. A null value indicates
	 * the absence of body content.
	 *
	 * @return null|array|object The deserialized body parameters, if any.
	 *     These will typically be an array or object.
	 */
	public function getParsedBody()
	{
		return $this->parsedBody;
	}

	/**
	 * Return an instance with the specified body parameters.
	 *
	 * These MAY be injected during instantiation.
	 *
	 * If the request Content-Type is either application/x-www-form-urlencoded
	 * or multipart/form-data, and the request method is POST, use this method
	 * ONLY to inject the contents of $_POST.
	 *
	 * The data IS NOT REQUIRED to come from $_POST, but MUST be the results of
	 * deserializing the request body content. Deserialization/parsing returns
	 * structured data, and, as such, this method ONLY accepts arrays or objects,
	 * or a null value if nothing was available to parse.
	 *
	 * As an example, if content negotiation determines that the request data
	 * is a JSON payload, this method could be used to create a request
	 * instance with the deserialized parameters.
	 *
	 * This method MUST be implemented in such a way as to retain the
	 * immutability of the message, and MUST return an instance that has the
	 * updated body parameters.
	 *
	 * @param null|array|object $data The deserialized body data. This will
	 *     typically be in an array or object.
	 * @return self
	 * @throws \InvalidArgumentException if an unsupported argument type is
	 *     provided.
	 */
	public function withParsedBody($data)
	{
		if ($data === $this->parsedBody)
		{
			return $this;
		}

		if ($data !== null && is_array($data) === false && is_object($data) === false)
		{
			throw new InvalidArgumentException('$data must be null, array, or an object');
		}

		$request = clone $this;
		$request->parsedBody = $data;

		return $request;
	}

	/**
	 * Retrieve attributes derived from the request.
	 *
	 * The request "attributes" may be used to allow injection of any
	 * parameters derived from the request: e.g., the results of path
	 * match operations; the results of decrypting cookies; the results of
	 * deserializing non-form-encoded message bodies; etc. Attributes
	 * will be application and request specific, and CAN be mutable.
	 *
	 * @return array Attributes derived from the request.
	 */
	public function getAttributes()
	{
		return $this->attributes;
	}

	/**
	 * Retrieve a single derived request attribute.
	 *
	 * Retrieves a single derived request attribute as described in
	 * getAttributes(). If the attribute has not been previously set, returns
	 * the default value as provided.
	 *
	 * This method obviates the need for a hasAttribute() method, as it allows
	 * specifying a default value to return if the attribute is not found.
	 *
	 * @see getAttributes()
	 * @param string $name The attribute name.
	 * @param mixed $default Default value to return if the attribute does not exist.
	 * @return mixed
	 */
	public function getAttribute($name, $default = null)
	{
		if (isset($this->attributes[$name]) === true)
		{
			return $this->attributes[$name];
		}
		else
		{
			return $default;
		}
	}

	/**
	 * Return an instance with the specified derived request attribute.
	 *
	 * This method allows setting a single derived request attribute as
	 * described in getAttributes().
	 *
	 * This method MUST be implemented in such a way as to retain the
	 * immutability of the message, and MUST return an instance that has the
	 * updated attribute.
	 *
	 * @see getAttributes()
	 * @param string $name The attribute name.
	 * @param mixed $value The value of the attribute.
	 * @return self
	 */
	public function withAttribute($name, $value)
	{
		if ($this->getAttribute($name) === $value)
		{
			return $this;
		}

		$request = clone $this;
		$request->attributes[$name] = $value;
		return $request;
	}

	/**
	 * Return an instance that removes the specified derived request attribute.
	 *
	 * This method allows removing a single derived request attribute as
	 * described in getAttributes().
	 *
	 * This method MUST be implemented in such a way as to retain the
	 * immutability of the message, and MUST return an instance that removes
	 * the attribute.
	 *
	 * @see getAttributes()
	 * @param string $name The attribute name.
	 * @return self
	 */
	public function withoutAttribute($name)
	{
		if (isset($this->attributes[$name]) === false)
		{
			return $this;
		}

		$request = clone $this;
		unset($request->attributes[$name]);
		return $request;
	}
}
