<?php namespace mef\Http\Test\Unit;

abstract class AbstractStreamTest extends \PHPUnit_Framework_TestCase
{
	protected $stream;

	/**
	 * @covers ::__toString
	 */
	public function testToString()
	{
		$this->stream->write('Hello, World!');
		$this->assertSame('Hello, World!', (string) $this->stream);
	}

	/**
	 * @covers ::close
	 * @covers ::__toString
	 */
	public function testCloseThenToString()
	{
		$this->stream->write('Hello, World!');
		$this->stream->close();
		$this->assertSame('', (string) $this->stream);
	}

	/**
	 * @covers ::close
	 * @expectedException \RuntimeException
	 */
	public function testDoubleClose()
	{
		$this->stream->close();
		$this->stream->close();
	}

	/**
	 * @covers ::detach
	 * @covers ::__toString
	 */
	public function testDetachThenToString()
	{
		$this->stream->write('Hello, World!');
		$this->stream->detach();
		$this->assertSame('', (string) $this->stream);
	}

	/**
	 * @covers ::tell
	 */
	public function testTell()
	{
		$data = 'Hello, World!';
		$this->assertSame(0, $this->stream->tell());
		$this->stream->write($data);

		$this->assertSame(strlen($data), $this->stream->tell());
	}

	/**
	 * @covers ::tell
	 * @expectedException \RuntimeException
	 */
	public function testCloseThenTell()
	{
		$this->stream->close();
		$this->stream->tell();
	}

	/**
	 * @covers ::eof
	 */
	public function testEof()
	{
		$this->assertFalse($this->stream->eof());
		$this->stream->read(1);
		$this->assertTrue($this->stream->eof());
	}

	/**
	 * @covers ::eof
	 * @covers ::close
	 */
	public function testCloseThenEof()
	{
		$this->assertFalse($this->stream->eof());
		$this->stream->close();
		$this->assertTrue($this->stream->eof());
	}

	/**
	 * @covers ::isSeekable
	 */
	public function testIsSeekable()
	{
		$this->assertTrue($this->stream->isSeekable());
	}

	/**
	 * @covers ::isSeekable
	 * @covers ::close
	 */
	public function testCloseThenIsSeekable()
	{
		$this->stream->close();
		$this->assertFalse($this->stream->isSeekable());
	}

	/**
	 * @covers ::seek
	 * @covers ::tell
	 */
	public function testSeek()
	{
		$data = 'Hello, World!';
		$this->stream->write($data);
		$this->stream->seek(0, SEEK_SET);
		$this->assertSame(0, $this->stream->tell());

		$this->stream->seek(0, SEEK_END);
		$this->assertSame(strlen($data), $this->stream->tell());

		$this->stream->seek(-strlen($data), SEEK_CUR);
		$this->assertSame(0, $this->stream->tell());
	}

	/**
	 * @covers ::seek
	 * @expectedException \RuntimeException
	 */
	public function testCloseThenSeek()
	{
		$this->stream->close();
		$this->stream->seek(0, SEEK_SET);
	}

	/**
	 * @covers ::seek
	 * @expectedException \RuntimeException
	 */
	public function testInvalidSeek()
	{
		$this->stream->seek(10, SEEK_SET);
	}

	/**
	 * @covers ::rewind
	 * @covers ::tell
	 */
	public function testRewind()
	{
		$data = 'Hello, World!';
		$this->stream->write($data);

		$this->stream->seek(0, SEEK_END);
		$this->assertSame(strlen($data), $this->stream->tell());

		$this->stream->rewind();
		$this->assertSame(0, $this->stream->tell());
	}

	/**
	 * @covers ::isWritable
	 */
	public function testIsWritable()
	{
		$this->assertTrue($this->stream->isWritable());
	}

	/**
	 * @covers ::isWritable
	 * @covers ::close
	 */
	public function testCloseThenIsWritable()
	{
		$this->stream->close();
		$this->assertFalse($this->stream->isWritable());
	}

	/**
	 * @covers ::write
	 */
	public function testWrite()
	{
		$data = 'Hello, World!';
		$this->assertSame(strlen($data), $this->stream->write($data));
	}

	/**
	 * @covers ::write
	 * @expectedException \RuntimeException
	 */
	public function testCloseThenWrite()
	{
		$data = 'Hello, World!';
		$this->stream->close();
		@$this->stream->write($data);
	}

	/**
	 * @covers ::isReadable
	 */
	public function testIsReadable()
	{
		$this->assertTrue($this->stream->isReadable());
	}

	/**
	 * @covers ::isReadable
	 * @covers ::close
	 */
	public function testCloseThenIsReadable()
	{
		$this->stream->close();
		$this->assertFalse($this->stream->isReadable());
	}

	/**
	 * @covers ::read
	 */
	public function testReadNothing()
	{
		$this->assertSame('', $this->stream->read(10));
	}

	/**
	 * @covers ::read
	 * @covers ::write
	 * @covers ::rewind
	 */
	public function testRead()
	{
		$data = 'Hello, World!';
		$this->assertSame(strlen($data), $this->stream->write($data));
		$this->stream->rewind();
		$this->assertSame($data, $this->stream->read(strlen($data)));
	}

	/**
	 * @covers ::read
	 * @covers ::close
	 * @expectedException \RuntimeException
	 */
	public function testCloseThenRead()
	{
		$this->stream->close();
		@$this->stream->read(1);
	}

	/**
	 * @covers ::getContents
	 */
	public function testEmptyGetContents()
	{
		$this->assertSame('', $this->stream->getContents());
	}

	/**
	 * @covers ::getContents
	 * @covers ::write
	 * @covers ::rewind
	 */
	public function testGetContents()
	{
		$data = 'Hello, World!';
		$this->assertSame(strlen($data), $this->stream->write($data));
		$this->stream->rewind();
		$this->assertSame($data, $this->stream->getContents());
	}

	/**
	 * @covers ::getContents
	 * @covers ::close
	 * @expectedException \RuntimeException
	 */
	public function testCloseThenGetContents()
	{
		$this->stream->close();
		@$this->stream->getContents();
	}

	/**
	 * @covers ::getMetadata
	 */
	public function testGetAllMetadata()
	{
		$this->assertTrue(is_array($this->stream->getMetadata()));
	}

	/**
	 * @covers ::getMetadata
	 */
	public function testGetInvalidMetadata()
	{
		$this->assertNull($this->stream->getMetadata('foo'));
	}

	/**
	 * @covers ::getMetadata
	 * @covers ::close
	 */
	public function testCloseThenGetMetadata()
	{
		$this->stream->close();
		$this->assertSame([], $this->stream->getMetadata());
	}

	/**
	 * @covers ::getSize
	 */
	public function testGetSize()
	{
		$data = 'Hello, World!';
		$this->assertSame(strlen($data), $this->stream->write($data));
		$this->assertSame(strlen($data), $this->stream->getSize());
	}

	/**
	 * @covers ::getSize
	 */
	public function testCloseThenGetSize()
	{
		$data = 'Hello, World!';
		$this->assertSame(strlen($data), $this->stream->write($data));
		$this->stream->close();
		$this->assertNull($this->stream->getSize());
	}

	/**
	 * @covers ::seek
	 *
	 * @expectedException \RuntimeException
	 */
	public function testInvalidSeekWhence()
	{
		$this->stream->write('Hello, World!');
		$this->stream->seek(0, 123456);
	}

}