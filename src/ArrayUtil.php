<?php namespace mef\Http;

/**
 * A private class to manage some array operations.
 *
 * This should not be used by external classes.
 */
class ArrayUtil
{
	/**
	 * Insert the same key for every leaf in an array and return the modified
	 * array.
	 *
	 * This is a helper function to change PHP's odd format for the $_FILES
	 * into something that makes sense.
	 *
	 * e.g.,
	 * in:  ['foo', 'bar'], 'test'
	 * out: [['test' => 'foo'], ['test' => 'bar']]
	 *
	 * This is done recursively so that every node is affected.
	 *
	 * @param  array  $data
	 * @param  string $key
	 *
	 * @return array
	 */
	public static function insertKeyAtLeaf(array $data, $key)
	{
		foreach ($data as $i => $value)
		{
			if (is_array($value) === true)
			{
				$data[$i] = self::insertKeyAtLeaf($value, $key);
			}
			else
			{
				$data[$i] = [$key => $value];
			}
		}

		return $data;
	}

	/**
	 * Return an iterator that iterates over every leaf of an array.
	 *
	 * This implementation purposefully iterates in order, although it
	 * shouldn't be relied upon.
	 *
	 * @param  array  $array
	 *
	 * @return \Iterator
	 */
	public static function getRecursiveIterator(array $array)
	{
		$stack = array_reverse($array);

		while ($stack !== [])
		{
			$current = array_pop($stack);

			if (is_array($current) === true)
			{
				foreach (array_reverse($current) as $value)
				{
					$stack[] = $value;
				}
			}
			else
			{
				yield $current;
			}
		}
	}
}