<?php namespace mef\Http\Test\Integration;

use GuzzleHttp\Client;

class WebTest extends \PHPUnit_Framework_TestCase
{
	public function setup()
	{
		$this->client = new Client(['base_uri' => 'http://localhost:8000/']);
	}

	public function testIndex()
	{
		$response = $this->client->get('/');
		$this->assertSame(200, $response->getStatusCode());
	}

	public function testUpload()
	{
		$response = $this->client->post('/', [
			'multipart' => [[
					'name' => 'name',
					'contents' => 'John Doe'
				], [
					'name'     => 'attachment[foo][c][]',
					'contents' => 'Wahh?',
					'filename' => 'foo.txt',
					'headers'  => [
             		   'Content-Type' => 'text/plain'
            		]
				],
			]
		]);

		$this->assertSame(200, $response->getStatusCode());
		$this->assertTrue(strpos($response->getBody(), 'foo.txt') !== false);
		$this->assertTrue(strpos($response->getBody(), 'John Doe') !== false);
	}

	public function testSecret()
	{
		$response = $this->client->get('/secret');
		$this->assertSame(200, $response->getStatusCode());
		$this->assertTrue(strpos($response->getBody(), 'secret') !== false);
	}
}