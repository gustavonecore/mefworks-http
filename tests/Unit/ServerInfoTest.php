<?php namespace mef\Http\Test\Unit;

use mef\Http\ServerInfo;
use mef\Http\Uri;

/**
 * @coversDefaultClass \mef\Http\ServerInfo
 */
class ServerInfoTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::getHttpMethod()
	 * @covers ::getUri()
	 * @covers ::getHttpHeaders()
	 * @covers ::getProtocolVersion()
	 */
	public function testConstructor()
	{
		$server = new ServerInfo([
			'REQUEST_METHOD' => 'GET',
			'HTTP_HOST' => 'localhost',
			'REQUEST_URI' => '/foo',
			'HTTP_HOST' => 'localhost',
			'SERVER_PROTOCOL' => 'HTTP/1.1'
		]);

		$this->assertSame('GET', $server->getHttpMethod());
		$this->assertTrue($server->getUri() instanceof Uri);
		$this->assertSame('http://localhost/foo', (string) $server->getUri());
		$this->assertSame(['HOST' => 'localhost'], $server->getHttpHeaders());
		$this->assertSame('1.1', $server->getProtocolVersion());
	}

	/**
	 * @covers ::__construct
	 * @covers ::getHttpMethod()
	 * @covers ::getUri()
	 * @covers ::getHttpHeaders()
	 * @covers ::getProtocolVersion()
	 */
	public function testEmptyServer()
	{
		$server = new ServerInfo([]);

		$this->assertSame('', $server->getHttpMethod());
		$this->assertTrue($server->getUri() instanceof Uri);
		$this->assertSame('http:', (string) $server->getUri());
		$this->assertSame([], $server->getHttpHeaders());
		$this->assertSame('', $server->getProtocolVersion());
	}

	/**
	 * @covers ::__construct
	 * @covers ::getHttpMethod()
	 * @covers ::getUri()
	 * @covers ::getHttpHeaders()
	 * @covers ::getProtocolVersion()
	 */
	public function testHttps()
	{
		$server = new ServerInfo([
			'HTTPS' => 'on',
		]);

		$this->assertTrue($server->getUri() instanceof Uri);
		$this->assertSame('https:', (string) $server->getUri());
	}

	/**
	 * Lighttpd uses CONTENT_TYPE, as opposed to HTTP_CONTENT_TYPE.
	 *
	 * @covers ::getHttpHeaders()
	 */
	public function testContentType()
	{
		$server = new ServerInfo([
			'CONTENT_TYPE' => 'application/json; charset=utf8',
		]);

		$this->assertSame(['CONTENT-TYPE' => 'application/json; charset=utf8'], $server->getHttpHeaders());
	}
}