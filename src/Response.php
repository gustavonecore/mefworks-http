<?php namespace mef\Http;

use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class Response extends Message implements ResponseInterface
{
	private $statusCode = 200;
	private $reasonPhrase = '';

	private static $defaultReasonPhrases = [
		100 => 'Continue',
		101 => 'Switching Protocols',
		200 => 'OK',
		201 => 'Created',
		202 => 'Accepted',
		203 => 'Non-Authoritative Information',
		204 => 'No Content',
		205 => 'Reset Content',
		206 => 'Partial Content',
		300 => 'Multiple Choices',
		301 => 'Move Permanently',
		302 => 'Found',
		303 => 'See Other',
		304 => 'Not Modified',
		305 => 'Use Proxy',
		307 => 'Temporary Redirect',
		400 => 'Bad Request',
		401 => 'Unauthorized',
		402 => 'Payment Required',
		403 => 'Forbidden',
		404 => 'Not Found',
		405 => 'Method Not Allowed',
		406 => 'Not Acceptable',
		407 => 'Proxy Authentication Required',
		408 => 'Request Timeout',
		409 => 'Conflict',
		410 => 'Gone',
		411 => 'Length Required',
		412 => 'Precondition Failed',
		413 => 'Payload Too Large',
		414 => 'URI Too Long',
		415 => 'Unsuppported Media Type',
		416 => 'Range Not Satisfiable',
		417 => 'Expectation Failed',
		426 => 'Upgrade Required',
		500 => 'Internal Server Error',
		501 => 'Not Implemented',
		502 => 'Bad Gateway',
		503 => 'Service Unavailable',
		504 => 'Gateway Timeout',
		505 => 'HTTP Version Not Supported'
	];

	/**
	 * Build and return a Response from components of an array.
	 *
	 * The array may contain:
	 *
	 * - body
	 * - headers
	 * - code
	 * - reasonPhrase
	 * - requestTarget
	 * - protocolVersion
	 *
	 * @param array  $components
	 *
	 * @return \mef\Http\Response
	 */
	private static function filterArray(array $components)
	{
		$components += [
			'code' => 200,
			'reasonPhrase' => ''
		];

		return $components;
	}

	/**
	 * Constructor
	 *
	 * @param array $properties An array with the following keys:
	 *   - body:            string|Psr\Http\Message\StreamInterface
	 *   - headers:         array   key/value headers
	 *   - protocolVersion: string  default '1.1'
	 *   - code:            int     default 200
	 *   - reasonPhrase     string
	 */
	public function __construct(array $properties)
	{
		parent::__construct($properties);

		$properties = self::filterArray($properties);

		$this->setStatus($properties['code'], $properties['reasonPhrase']);
	}

	/**
	 * Gets the response status code.
	 *
	 * The status code is a 3-digit integer result code of the server's attempt
	 * to understand and satisfy the request.
	 *
	 * @return int Status code.
	 */
	public function getStatusCode()
	{
		return $this->statusCode;
	}

	/**
	 * Return an instance with the specified status code and, optionally, reason phrase.
	 *
	 * If no reason phrase is specified, implementations MAY choose to default
	 * to the RFC 7231 or IANA recommended reason phrase for the response's
	 * status code.
	 *
	 * This method MUST be implemented in such a way as to retain the
	 * immutability of the message, and MUST return an instance that has the
	 * updated status and reason phrase.
	 *
	 * @link http://tools.ietf.org/html/rfc7231#section-6
	 * @link http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
	 * @param int $code The 3-digit integer result code to set.
	 * @param string $reasonPhrase The reason phrase to use with the
	 *     provided status code; if none is provided, implementations MAY
	 *     use the defaults as suggested in the HTTP specification.
	 * @return self
	 * @throws \InvalidArgumentException For invalid status code arguments.
	 */
	public function withStatus($code, $reasonPhrase = '')
	{
		$code = (int) $code;
		$reasonPhrase = (string) $reasonPhrase;

		if ($code === $this->statusCode && $reasonPhrase === $this->reasonPhrase)
		{
			return $this;
		}

		$response = clone $this;
		$response->setStatus($code, $reasonPhrase);

		return $response;
	}


	/**
	 * See ::withStatus
	 *
	 * @param int $code The 3-digit integer result code to set.
	 * @param string $reasonPhrase The reason phrase to use with the
	 *     provided status code; if none is provided, implementations MAY
	 *     use the defaults as suggested in the HTTP specification.
	 *
	 *
	 * @throws \InvalidArgumentException For invalid status code arguments.
	 */
	private function setStatus($code, $reasonPhrase = '')
	{
		$code = (int) $code;
		$reasonPhrase = (string) $reasonPhrase;

		if ($code < 100 || $code > 999)
		{
			throw new InvalidArgumentException('$code must be a three digit integer');
		}

		$this->statusCode = $code;
		$this->reasonPhrase = $reasonPhrase;
	}

	/**
	 * Gets the response reason phrase associated with the status code.
	 *
	 * Because a reason phrase is not a required element in a response
	 * status line, the reason phrase value MAY be null. Implementations MAY
	 * choose to return the default RFC 7231 recommended reason phrase (or those
	 * listed in the IANA HTTP Status Code Registry) for the response's
	 * status code.
	 *
	 * @link http://tools.ietf.org/html/rfc7231#section-6
	 * @link http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
	 * @return string Reason phrase; must return an empty string if none present.
	 */
	public function getReasonPhrase()
	{
		if ($this->reasonPhrase === '' && isset(self::$defaultReasonPhrases[$this->statusCode]) === true)
		{
			return self::$defaultReasonPhrases[$this->statusCode];
		}
		else
		{
			return $this->reasonPhrase;
		}
	}
}
