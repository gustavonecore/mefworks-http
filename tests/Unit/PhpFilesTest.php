<?php namespace mef\Http\Test\Unit;

use \mef\Http\PhpFiles;
use \mef\Http\UploadedFile;

/**
 * @coversDefaultClass \mef\Http\PhpFiles
 */
class PhpFilesTest extends \PHPUnit_Framework_TestCase
{
	private $emptyFile = [
		'tmp_name' => '',
		'name' => '',
		'size' => 0,
		'error' => 0,
		'type' => ''
	];

	private $twoIndexedFiles = [
		'tmp_name' => ['', ''],
		'name' => ['', ''],
		'size' => [0, 0],
		'error' => [0, 0],
		'type' => ['', ''],
	];

	/**
	 * @covers ::__construct
	 * @covers ::getUploadedFiles
	 * @covers ::parsePhpFiles
	 */
	public function testParseOneSimpleFile()
	{
		$files = new PhpFiles(['file' => $this->emptyFile]);

		$this->assertSame(1, count($files->getUploadedFiles()));
		$this->assertTrue($files->getUploadedFiles()['file'] instanceof UploadedFile);
	}

	/**
	 * @covers ::__construct
	 * @covers ::getUploadedFiles
	 * @covers ::parsePhpFiles
	 */
	public function testParseTwoSimpleFiles()
	{
		$files = new PhpFiles([
			'file1' => $this->emptyFile,
			'file2' => $this->emptyFile
		]);

		$this->assertSame(2, count($files->getUploadedFiles()));
		$this->assertTrue($files->getUploadedFiles()['file1'] instanceof UploadedFile);
		$this->assertTrue($files->getUploadedFiles()['file2'] instanceof UploadedFile);
	}

	/**
	 * @covers ::__construct
	 * @covers ::getUploadedFiles
	 * @covers ::parsePhpFiles
	 */
	public function testComplicatedFiles()
	{
		$files = new PhpFiles([
			'file' => $this->twoIndexedFiles
		]);

		$this->assertSame(2, count($files->getUploadedFiles()['file']));
		$this->assertTrue($files->getUploadedFiles()['file'][0] instanceof UploadedFile);
		$this->assertTrue($files->getUploadedFiles()['file'][1] instanceof UploadedFile);
	}

	/**
	 * @covers ::__construct
	 * @covers ::getUploadedFiles
	 * @covers ::parsePhpFiles
	 */
	public function testNestedFiles()
	{
		$files = new PhpFiles([
			'file' => [
				'bar' => $this->twoIndexedFiles
			]
		]);

		$this->assertSame(2, count($files->getUploadedFiles()['file']['bar']));
		$this->assertTrue($files->getUploadedFiles()['file']['bar'][0] instanceof UploadedFile);
		$this->assertTrue($files->getUploadedFiles()['file']['bar'][1] instanceof UploadedFile);
	}

	/**
	 * @covers ::__construct
	 * @covers ::getIterator
	 * @covers ::parsePhpFiles
	 */
	public function testIterateOverComplicatedFiles()
	{
		$files = new PhpFiles([
			'file' => $this->twoIndexedFiles
		]);

		$this->assertSame(2, count(iterator_to_array($files)));
	}

	/**
	 * @covers ::__construct
	 * @covers ::parsePhpFiles
	 *
	 * @expectedException \RuntimeException
	 */
	public function testNotAnArray()
	{
		new PhpFiles([1]);
	}

	/**
	 * @covers ::__construct
	 * @covers ::parsePhpFiles
	 *
	 * @expectedException \RuntimeException
	 */
	public function testInvalidArray()
	{
		$this->emptyFile['size'] = '';
		new PhpFiles(['file' => $this->emptyFile]);
	}
}