<?php namespace mef\Http\Test\Unit;

use mef\Http\Uri;

/**
 * @coversDefaultClass \mef\Http\Uri
 */
class UriTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::fromString
	 * @covers ::__construct
	 */
	public function testFullUriFromString()
	{
		$uri = Uri::fromString('scheme://user:password@host:42/path?param1=val1&param2=val2#fragment');
		$this->assertTrue($uri instanceof Uri);

		$this->assertSame('scheme', $uri->getScheme());
		$this->assertSame('user:password@host:42', $uri->getAuthority());
		$this->assertSame('user:password', $uri->getUserInfo());
		$this->assertSame('host', $uri->getHost());
		$this->assertSame(42, $uri->getPort());
		$this->assertSame('/path', $uri->getPath());
		$this->assertSame('param1=val1&param2=val2', $uri->getQuery());
		$this->assertSame('fragment', $uri->getFragment());
	}

	/**
	 * @covers ::getScheme
	 */
	public function testDefaultEmptyScheme()
	{
		$uri = new Uri;
		$this->assertSame('', $uri->getScheme());
	}

	/**
	 * @covers ::getScheme
	 * @covers ::withScheme
	 */
	public function testWithNewScheme()
	{
		$uri = new Uri;
		$uri = $uri->withScheme('http');

		$this->assertTrue($uri instanceof Uri);
		$this->assertSame('http', $uri->getScheme());
	}

	/**
	 * @covers ::getScheme
	 * @covers ::withScheme
	 */
	public function testWithSameScheme()
	{
		$uri = new Uri;
		$uri = $uri->withScheme('http');
		$uri2 = $uri->withScheme('http');

		$this->assertTrue($uri instanceof Uri);
		$this->assertTrue($uri2 instanceof Uri);
		$this->assertSame('http', $uri2->getScheme());
		$this->assertSame($uri, $uri2);
	}

	/**
	 * @covers ::getAuthority
	 */
	public function testEmptyAuthority()
	{
		$uri = new Uri;

		$this->assertSame('', $uri->getAuthority());
	}

	/**
	 * @covers ::getAuthority
	 */
	public function testGetAuthorityWithoutPort()
	{
		$uri = Uri::fromString('http://localhost/');

		$this->assertSame('localhost', $uri->getAuthority());
	}

	/**
	 * @covers ::getAuthority
	 */
	public function testGetAuthorityWithPort()
	{
		$uri = Uri::fromString('http://localhost:8000/');

		$this->assertSame('localhost:8000', $uri->getAuthority());
	}

	/**
	 * @covers ::getAuthority
	 */
	public function testGetAuthorityWithDefaultPort()
	{
		$uri = Uri::fromString('http://localhost:80/');

		$this->assertSame('localhost', $uri->getAuthority());
	}

	/**
	 * @covers ::getAuthority
	 */
	public function testGetAuthorityWithUserInfo()
	{
		$uri = Uri::fromString('http://user:password@localhost/');

		$this->assertSame('user:password@localhost', $uri->getAuthority());
	}

	/**
	 * @covers ::getUserInfo
	 */
	public function testGetDefaultUserInfo()
	{
		$uri = new Uri;

		$this->assertSame('', $uri->getUserInfo());
	}

	/**
	 * @covers ::getUserInfo
	 */
	public function testGetUserInfoWithoutPassword()
	{
		$uri = Uri::fromString('http://user@localhost/');

		$this->assertSame('user', $uri->getUserInfo());
	}

	/**
	 * @covers ::getUserInfo
	 */
	public function testGetUserInfoWithPassword()
	{
		$uri = Uri::fromString('http://user:password@localhost/');

		$this->assertSame('user:password', $uri->getUserInfo());
	}

	/**
	 * @covers ::getUserInfo
	 */
	public function testGetUserInfoWithEmptyPassword()
	{
		$uri = Uri::fromString('http://user:@localhost/');

		$this->assertSame('user', $uri->getUserInfo());
	}

	/**
	 * @covers ::getUserInfo
	 * @covers ::withUserInfo
	 */
	public function testSetUser()
	{
		$uri = new Uri;
		$uri = $uri->withUserInfo('user');

		$this->assertTrue($uri instanceof Uri);
		$this->assertSame('user', $uri->getUserInfo());
	}

	/**
	 * @covers ::getUserInfo
	 * @covers ::withUserInfo
	 */
	public function testSetUserAndPassword()
	{
		$uri = new Uri;
		$uri = $uri->withUserInfo('user', 'password');

		$this->assertTrue($uri instanceof Uri);
		$this->assertSame('user:password', $uri->getUserInfo());
	}

	/**
	 * @covers ::getUserInfo
	 * @covers ::withUserInfo
	 */
	public function testSetSameUserAndPassword()
	{
		$uri = new Uri;
		$uri = $uri->withUserInfo('user', 'password');
		$uri2 = $uri->withUserInfo('user', 'password');

		$this->assertTrue($uri instanceof Uri);
		$this->assertTrue($uri2 instanceof Uri);
		$this->assertSame('user:password', $uri2->getUserInfo());
		$this->assertSame($uri, $uri2);
	}

	/**
	 * @covers ::getHost
	 */
	public function testGetDefaultHost()
	{
		$uri = new Uri;

		$this->assertSame('', $uri->getHost());
	}

	/**
	 * @covers ::getHost
	 * @covers ::withHost
	 */
	public function testSetHost()
	{
		$uri = new Uri;
		$uri = $uri->withHost('localhost');

		$this->assertTrue($uri instanceof Uri);
		$this->assertSame('localhost', $uri->getHost());
	}

	/**
	 * @covers ::getHost
	 * @covers ::withHost
	 */
	public function testSetSameHost()
	{
		$uri = new Uri;
		$uri = $uri->withHost('localhost');
		$uri2 = $uri->withHost('localhost');

		$this->assertTrue($uri instanceof Uri);
		$this->assertTrue($uri2 instanceof Uri);
		$this->assertSame('localhost', $uri2->getHost());
		$this->assertSame($uri, $uri2);
	}

	/**
	 * @covers ::getPort
	 */
	public function testGetDefaultPort()
	{
		$uri = new Uri;

		$this->assertNull($uri->getPort());
	}

	/**
	 * @covers ::getPort
	 */
	public function testSetDefaultSchemaPort()
	{
		$uri = Uri::fromString('http://localhost:80/');

		$this->assertTrue($uri instanceof Uri);
		$this->assertNull($uri->getPort());
	}

	/**
	 * @covers ::getPort
	 */
	public function testSetDifferentSchemaPort()
	{
		$uri = Uri::fromString('http://localhost:8000/');

		$this->assertTrue($uri instanceof Uri);
		$this->assertSame(8000, $uri->getPort());
	}

	/**
	 * @covers ::getPort
	 * @covers ::withPort
	 */
	public function testWithPort()
	{
		$uri = new Uri;
		$uri = $uri->withPort(42);

		$this->assertTrue($uri instanceof Uri);
		$this->assertSame(42, $uri->getPort());
	}

	/**
	 * @covers ::getPort
	 * @covers ::withPort
	 */
	public function testWithSamePort()
	{
		$uri = new Uri;
		$uri = $uri->withPort(42);
		$uri2 = $uri->withPort(42);

		$this->assertTrue($uri instanceof Uri);
		$this->assertTrue($uri2 instanceof Uri);
		$this->assertSame(42, $uri2->getPort());
		$this->assertSame($uri, $uri2);
	}

	/**
	 * @covers ::getPath
	 */
	public function testGetDefaultPath()
	{
		$uri = new Uri;

		$this->assertSame('', $uri->getPath());
	}

	/**
	 * @covers ::getPath
	 * @covers ::withPath
	 */
	public function testSetPath()
	{
		$uri = new Uri;
		$uri = $uri->withPath('foo');

		$this->assertTrue($uri instanceof Uri);
		$this->assertSame('foo', $uri->getPath());
	}

	/**
	 * @covers ::getPath
	 * @covers ::withPath
	 */
	public function testSetSamePath()
	{
		$uri = new Uri;
		$uri = $uri->withPath('foo');
		$uri2 = $uri->withPath('foo');

		$this->assertTrue($uri instanceof Uri);
		$this->assertTrue($uri2 instanceof Uri);
		$this->assertSame('foo', $uri2->getPath());
		$this->assertSame($uri, $uri2);
	}

	/**
	 * @covers ::getQuery
	 */
	public function testGetDefaultQuery()
	{
		$uri = new Uri;

		$this->assertSame('', $uri->getQuery());
	}

	/**
	 * @covers ::getQuery
	 * @covers ::withQuery
	 */
	public function testSetQuery()
	{
		$uri = new Uri;
		$uri = $uri->withQuery('foo=bar');

		$this->assertTrue($uri instanceof Uri);
		$this->assertSame('foo=bar', $uri->getQuery());
	}

	/**
	 * @covers ::getQuery
	 * @covers ::withQuery
	 */
	public function testSetSameQuery()
	{
		$uri = new Uri;
		$uri = $uri->withQuery('foo=bar');
		$uri2 = $uri->withQuery('foo=bar');

		$this->assertTrue($uri instanceof Uri);
		$this->assertTrue($uri2 instanceof Uri);
		$this->assertSame('foo=bar', $uri2->getQuery());
		$this->assertSame($uri, $uri2);
	}

	/**
	 * @covers ::getFragment
	 */
	public function testGetDefaultFragment()
	{
		$uri = new Uri;

		$this->assertSame('', $uri->getFragment());
	}

	/**
	 * @covers ::getFragment
	 * @covers ::withFragment
	 */
	public function testSetFragment()
	{
		$uri = new Uri;
		$uri = $uri->withFragment('fragment');

		$this->assertTrue($uri instanceof Uri);
		$this->assertSame('fragment', $uri->getFragment());
	}

	/**
	 * @covers ::getFragment
	 * @covers ::withFragment
	 */
	public function testSetSameFragment()
	{
		$uri = new Uri;
		$uri = $uri->withFragment('fragment');
		$uri2 = $uri->withFragment('fragment');

		$this->assertTrue($uri instanceof Uri);
		$this->assertTrue($uri2 instanceof Uri);
		$this->assertSame('fragment', $uri2->getFragment());
		$this->assertSame($uri, $uri2);
	}

	/**
	 * @covers ::__toString
	 */
	public function testFullString()
	{
		$string = 'scheme://user:password@host:42/path?param1=val1&param2=val2#fragment';
		$uri = Uri::fromString($string);

		$this->assertSame($string, (string) $uri);
	}

	/**
	 * @covers ::__toString
	 */
	public function testToStringWithOnlyScheme()
	{
		$uri = (new Uri)->withScheme('http');

		$this->assertSame('http:', (string) $uri);
	}

	/**
	 * @covers ::__toString
	 */
	public function testToStringWithAuthorityAndAbsolutePath()
	{
		$uri = (new Uri)->withHost('localhost')->withPath('foo');

		$this->assertSame('//localhost/foo', (string) $uri);
	}

	/**
	 * @covers ::__toString
	 */
	public function testToStringWithoutAuthorityWithPath()
	{
		$uri = (new Uri)->withPath('/foo');

		$this->assertSame('/foo', (string) $uri);
	}

	/**
	 * @covers ::__toString
	 */
	public function testToStringWithoutAuthorityWithRelativePath()
	{
		$uri = (new Uri)->withPath('foo');

		$this->assertSame('/foo', (string) $uri);
	}

	/**
	 * @covers ::setScheme
	 */
	public function testValidScheme()
	{
		$uri = new Uri('http');
		$this->assertSame('http', $uri->getScheme());
	}

	/**
	 * The first parameter to Uri is the scheme, not a full URI string.
	 * This (maybe) common mistake should throw an invalid argument exception.
	 *
	 * @covers ::setScheme
	 *
	 * @expectedException \InvalidArgumentException
	 */
	public function testInvalidScheme()
	{
		$uri = new Uri('http://localhost/');
	}

	/**
	 * @covers ::fromString
	 *
	 * @expectedException \InvalidArgumentException
	 */
	public function testInvalidUriString()
	{
		URI::fromString('http://');
	}
}