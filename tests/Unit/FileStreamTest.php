<?php namespace mef\Http\Test\Unit;

use mef\Http\FileStream;

/**
 * @coversDefaultClass \mef\Http\FileStream
 */
class FileStreamTest extends AbstractStreamTest
{
	protected $fp;

	public function setup()
	{
		$this->fp = fopen('php://memory', 'rw');
		$this->stream = new FileStream($this->fp);
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$this->assertTrue($this->stream instanceof FileStream);
	}

	/**
	 * @covers ::detach
	 * @covers ::__toString
	 */
	public function testResourceStillExistsAfterDetach()
	{
		$this->stream->write('Hello, World!');
		$this->stream->detach();

		fseek($this->fp, 0, SEEK_SET);
		$this->assertSame('Hello, World!', fgets($this->fp, 1000));
	}

	/**
	 * @covers ::detach
	 */
	public function testResourceIsReturnedByDetach()
	{
		$this->stream->write('Hello, World!');
		$fp = $this->stream->detach();

		$this->assertTrue(is_resource($fp));

		fseek($fp, 0, SEEK_SET);
		$this->assertSame('Hello, World!', fgets($fp, 1000));
	}

	/**
	 * @covers ::getSize
	 */
	public function testGetSize()
	{
		$this->assertNull($this->stream->getSize());
	}

	/**
	 * @covers ::write
	 * @expectedException \RuntimeException
	 */
	public function testCloseUnderlyingStreamThenWrite()
	{
		$data = 'Hello, World!';
		fclose($this->fp);
		@$this->stream->write($data);
	}

	/**
	 * @covers ::getMetadata
	 */
	public function testGetMetadata()
	{
		$this->assertSame('php://memory', $this->stream->getMetadata('uri'));
	}
}