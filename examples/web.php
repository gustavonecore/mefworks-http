<?php namespace mef\Http\Test;

require_once __DIR__ . '/../vendor/autoload.php';

use mef\Http\ServerRequest;
use mef\Http\Response;
use mef\Http\FileStream;
use mef\Http\StringStream;

ini_set('xdebug.var_display_max_depth', -1);

if (PHP_SAPI !== 'cli-server')
{
	echo 'Must run this as a CLI server', PHP_EOL;
	echo 'php -S 0.0.0.0:8000 ', __FILE__, PHP_EOL;
	die();
}

// Define routes

$controllers = [
	'GET' => [
		'/' => function($request) {
			echo '
				<!doctype html>
				<html>
				<body>
				<form method="post" enctype="multipart/form-data">
				<label>Your Name: <input name="name"></label><br>
				<label>Attachment: <input type="file" name="attachment[foo][a][bar]"><br>
				<label>Attachment: <input type="file" name="attachment[foo][a][zed]"><br>
				<label>Attachment: <input type="file" name="attachment[foo][b][zed]"><br>
				<label>Attachment: <input type="file" name="attachment[foo][c][]"><br>
				<label>Attachment: <input type="file" name="attachment[foo][c][]"><br>
				<input type="submit" value="Submit">
				</form>
				</body>
				</html>
			';
		},

		'/secret' => function ($request) {
			echo '<p>You found the secret page.</p>';
		}
	],

	'POST' => [
		'/' => function($request) {
			$input = $request->getParsedBody() + ['name' => ''];

			echo '<p>Hello, ', $input['name'] ?: 'Anonymous', '!</p>';

			$uploadCount = 0;
			$files = $request->getUploadedFiles();

			array_walk_recursive($files, function ($file) use (&$uploadCount) {
				if ($file->getError() === UPLOAD_ERR_OK)
				{
					$uploadCount++;
				}
			});

			if ($uploadCount === 0)
			{
				echo '<p>You did not upload a file.</p>';
			}
			else
			{
				echo '<p>You uploaded the following files:</p>';
				echo '<ul>';
				array_walk_recursive($files, function ($file) {
					if ($file->getError() === UPLOAD_ERR_OK)
					{
						echo '<li>', htmlspecialchars($file->getClientFilename()), '</li>';
					}
				});
				echo '</ul>';
			}

			echo '<p><a href="/">Try Again</a></p>';
		}
	]
];

// bootstrap
$request = ServerRequest::fromGlobals(
	new FileStream(fopen('php://input', 'r')),
	$_SERVER,
	$_GET,
	$_POST,
	$_COOKIE,
	$_FILES
);

$method = $request->getMethod();
$path = $request->getUri()->getPath();

// dispatch request

if (isset($controllers[$method][$path]) === false)
{
	$response = Response(['body' => new StringStream('Not Found')]);
	$response = $response->withStatus(404);
}
else
{
	ob_start();
	$controllers[$method][$path]($request);
	$response = new Response(['body' => new StringStream(trim(ob_get_clean()))]);
}

// output response

http_response_code($response->getStatusCode());
foreach ($response->getHeaders() as $key => $values)
{
	foreach ($values as $i => $value)
	{
		header("$key: $value", $i === 0);
	}
}
echo $response->getBody()->getContents();